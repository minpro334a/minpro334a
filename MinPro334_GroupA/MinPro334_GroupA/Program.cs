using Microsoft.EntityFrameworkCore;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllersWithViews();

//add services
builder.Services.AddScoped<AuthService>();
builder.Services.AddScoped<BankService>();
builder.Services.AddScoped<CaraPembayaranService>();
builder.Services.AddScoped<CategoryProductKesehatanService>();
builder.Services.AddScoped<SegmentationProductKesehatanService>();
builder.Services.AddScoped<MedicalItemService>();
builder.Services.AddScoped<ObatService>();
builder.Services.AddScoped<PasienProfilService>();
builder.Services.AddScoped<TambahSpesialisasiService>();
builder.Services.AddScoped<SpecializationService>();
builder.Services.AddScoped<DokterService>();

builder.Services.AddScoped<ProfilDokterServices>();
builder.Services.AddScoped<TindakanDokterServices>();
builder.Services.AddScoped<LokasiServices>();
builder.Services.AddScoped<LevelServices>();

builder.Services.AddScoped<AturAksesMenuService>();
builder.Services.AddScoped<HakAksesService>();
builder.Services.AddScoped<AlamatService>();
builder.Services.AddScoped<LokasiService>();
builder.Services.AddScoped<PasienService>();
builder.Services.AddScoped<HubunganPasienService>();
builder.Services.AddScoped<GolonganDarahService>();



// ADD SESSION
builder.Services.AddDistributedMemoryCache();
builder.Services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
builder.Services.AddSession(option =>
{
    option.IdleTimeout = TimeSpan.FromHours(1);
    option.Cookie.HttpOnly = true;
    option.Cookie.IsEssential = true;
});

// Add connection string
builder.Services.AddDbContext<DB_SpecificationContext>(option =>
{
    option.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();
app.UseSession();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
