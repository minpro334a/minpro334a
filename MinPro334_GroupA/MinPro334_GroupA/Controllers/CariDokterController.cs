﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class CariDokterController : Controller
    {
        private CariDokterServices cariDokterServices;
        private ProfilDokterServices profilDokterServices;
        private readonly IWebHostEnvironment webHostEnvironment;
        private int IdUser = 1;
        public IActionResult Index()
        {
            return View();
        }
        public CariDokterController(CariDokterServices _cariDokterServices, ProfilDokterServices _profilDokterServices , IWebHostEnvironment _webHostEnvironment)
        {
            cariDokterServices = _cariDokterServices;
            profilDokterServices = _profilDokterServices;
            webHostEnvironment = _webHostEnvironment;
        }

        public async Task<IActionResult> Index(int id)
        {
            VMTblProfilDokter data = await cariDokterServices.GetDataById(id);
            return View();
        }
    }
}
