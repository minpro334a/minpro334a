﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class TindakanDokterController : Controller
    {
        private TindakanDokterServices tindakanDokterServices;
        private readonly IWebHostEnvironment webHostEnvironment;
        private int IdUser = 1;

        public TindakanDokterController(TindakanDokterServices _tindakanDokterServices, IWebHostEnvironment _webHostEnvironment)
        {
            tindakanDokterServices = _tindakanDokterServices;
            webHostEnvironment = _webHostEnvironment;
        }

        public async Task<IActionResult> Index()
        {
            VMTblProfilDokter data = await tindakanDokterServices.GetDataByIdForTreatment();
            return View(data);
        }

        public IActionResult Create()
        {
            TDoctorTreatment data = new TDoctorTreatment();
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(TDoctorTreatment dataParam)
        {
            VMResponse respon = await tindakanDokterServices.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return PartialView(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExist(string name, int id)
        {
            bool isExist = await tindakanDokterServices.CheckTreatmentByName(name, id);
            return Json(isExist);
        }

        public async Task<IActionResult> Delete(int id)
        {
            TDoctorTreatment data = await tindakanDokterServices.GetDataByIdTreatment(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await tindakanDokterServices.Delete(id);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("Index");
        }

    }
}
