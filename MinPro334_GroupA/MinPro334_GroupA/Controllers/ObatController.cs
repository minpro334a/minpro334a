﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Controllers
{
    public class ObatController : Controller
    {
        private MedicalItemService medicalItemService;
        private CategoryProductKesehatanService categoryProductKesehatanService;
        private ObatService obatService;
        private readonly ILogger<HomeController> _logger;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IWebHostEnvironment webHostEnvironment;

        public ObatController(MedicalItemService _medicalItemService, IWebHostEnvironment _webHostEnvironment, IHttpContextAccessor httpContextAccessor, ILogger<HomeController> logger, ObatService _obatService, CategoryProductKesehatanService _categoryProductKesehatanService)
        {
            medicalItemService = _medicalItemService;
            obatService = _obatService;
            categoryProductKesehatanService = _categoryProductKesehatanService;
            webHostEnvironment = _webHostEnvironment;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> Catalog(VMSearchPage dataSearch, VMSearchMenu searchMenu, string NameCategory, string nameProduct, string caution, int minAmount, int maxAmount, string text)
        {

            List<VMMMedicalItem> dataProduct = await medicalItemService.GetAllData();
            dataSearch.MinAmount = dataSearch.MinAmount == null ? decimal.MinValue : dataSearch.MinAmount;
            dataSearch.MaxAmount = dataSearch.MaxAmount == null ? decimal.MaxValue : dataSearch.MaxAmount;
            //dataSearch.MaxAmount = dataSearch.MaxAmount ?? decimal.MaxValue;//jika null data decimal jika tidak maka data searchnya sendiri

            if (nameProduct != null)
            {
                dataProduct = dataProduct.Where(a => a.Name.ToLower().Contains(nameProduct.ToLower())).ToList();
                searchMenu.Name = nameProduct;
            }
            if (minAmount != 0 && maxAmount != 0)
            {
                dataProduct = dataProduct.Where(a => a.PriceMin >= minAmount
                              && a.PriceMax <= maxAmount).ToList();
                searchMenu.PriceMin = minAmount;
                searchMenu.PriceMax = maxAmount;
            }
            if (caution != null)
            {
                dataProduct = dataProduct.Where(a => a.Caution.ToLower().Contains(caution)).ToList();
                searchMenu.Caution = caution;
            }
            if (NameCategory != null)
            {
                dataProduct = dataProduct.Where(a => a.NameCategory == NameCategory).ToList();
                searchMenu.NameCategory = NameCategory;
            }

            //Get session in VMOrderHeader first load
            VMMedicalItemPurchaseDetail dataHeader = HttpContext.Session.GetComplexData<VMMedicalItemPurchaseDetail>("ListCart");
            if (dataHeader == null)
            {
                dataHeader = new VMMedicalItemPurchaseDetail();
                dataHeader.ListDetails = new List<VMMedicalItemPurchaseDetail>();
            }

            var ListDetail = JsonConvert.SerializeObject(dataHeader.ListDetails);
            ViewBag.dataHeader = dataHeader;
            ViewBag.dataDetail = ListDetail;

            ViewBag.searchMenu = searchMenu;
            ViewBag.Search = dataSearch;
            ViewBag.CurrentPageSize = dataSearch.pageSize;

            return View(PaginatedList<VMMMedicalItem>.CreateAsync(dataProduct, dataSearch.pageNumber ?? 1, dataSearch.pageSize ?? 4));
        }

        public async Task<IActionResult> SearchMenu()
        {

            List<MMedicalItemCategory> listCategory = await categoryProductKesehatanService.GetAllData();
            ViewBag.listCategory = listCategory;

            return PartialView();
        }

        [HttpPost]
        public JsonResult SetSession(VMMedicalItemPurchaseDetail dataHeader)
        {
            HttpContext.Session.SetComplexData("ListCart", dataHeader);
            return Json("");
        }

        //remove session
        public JsonResult RemoveSession()
        {
            HttpContext.Session.Remove("ListCart");
            //HttpContext.Session.Clear(); clear all session
            return Json("");
        }

    }
}
