﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace MinPro334_GroupA.Controllers
{
    public class DokterController : Controller
    {
        private DokterService dokterService;
        private int IdUser = 1;
        public DokterController(DokterService _dokterService)
        {
            dokterService = _dokterService;
        }

        public async Task<IActionResult> Catalog(VMSearchPage dataSearch)
        {
            List<VMCariDokter> data = await dokterService.CariAllData();
            // if (!string.IsNullOrEmpty(dataSearch.Specialization_Name))
            if (dataSearch.Specialization_Name != null)
            {
                data = data.Where(a => a.Specialization_Name.ToLower().Contains(dataSearch.Specialization_Name.ToLower())).ToList();
            }
            if (dataSearch.Fullname != null)
            {
                data = data.Where(a => a.Fullname.ToLower().Contains(dataSearch.Fullname.ToLower())).ToList();
            }
            //if (dataSearch.Location_Name != null)
            //{
            //    data = data.Where(a => a.Location_Name.ToLower().Contains(dataSearch.Location_Name.ToLower())).ToList();
            //}
            if (dataSearch.Location_Name != null) // Assuming locationName is the search criteria
            {
                data = data.Where(item => item.ListMedicalFacility.Any(child => child.Location_Name.ToLower().Contains(dataSearch.Location_Name.ToLower()))).ToList();
            }
            if (dataSearch.Treatment_Name != null)
            {
                data = data.Where(a => a.Treatment_Name.ToLower().Contains(dataSearch.Treatment_Name.ToLower())).ToList();
            }
            if(dataSearch.Abbreviation != null)
            {
                data = data.Where(item => item.ListMedicalFacility.Any(child => child.Abbreviation.ToLower().Contains(dataSearch.Abbreviation.ToLower()))).ToList();
            }

            
            ViewBag.Search = dataSearch;
            ViewBag.CurrentPageSize = dataSearch.pageSize;

            return View(PaginatedList<VMCariDokter>.CreateAsync(data, dataSearch.pageNumber ?? 1, dataSearch.pageSize ?? 4));
        }

        public async Task<IActionResult> CariDokterdeh()
        {
            //List<VMMBiodata> dataProduct = await dokterService.GetAllData();
            //ViewBag.ListCategory = dataProduct;

            List<MLocation> dataLocation = await dokterService.GetAllDataLokasi();
            ViewBag.ListLocation = dataLocation;

            List<MSpecialization> dataSpecialization = await dokterService.GetAllDataSpecialization();
            ViewBag.ListSpecialization = dataSpecialization;

            List<TDoctorTreatment> dataDoctorTreatment = await dokterService.GetAllDataTreatment();
            ViewBag.ListDoctorTreatment = dataDoctorTreatment;


            List<MLocationLevel> dataLocationLevel = await dokterService.GetAllDataLocationLevel();
            ViewBag.ListLocationLevel = dataLocationLevel;
            return PartialView();
        }
        public async Task<IActionResult> CariDokter()
        {
            //List<VMMBiodata> dataProduct = await dokterService.GetAllData();
            //ViewBag.ListCategory = dataProduct;

            List<MLocation> dataLocation = await dokterService.GetAllDataLokasi();
            ViewBag.ListLocation = dataLocation;

            List<MSpecialization> dataSpecialization = await dokterService.GetAllDataSpecialization();
            ViewBag.ListSpecialization = dataSpecialization;

            List<TDoctorTreatment> dataDoctorTreatment = await dokterService.GetAllDataTreatment();
            ViewBag.ListDoctorTreatment = dataDoctorTreatment;

            List<MLocationLevel> dataLocationLevel = await dokterService.GetAllDataLocationLevel();
            ViewBag.ListLocationLevel = dataLocationLevel;

            return View();
        }
    }
}
