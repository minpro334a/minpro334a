﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class ProfilDokterController : Controller
    {
        private ProfilDokterServices profilDokterServices;

        private readonly IWebHostEnvironment webHostEnvironment;
        private int IdUser = 1;

        public ProfilDokterController( ProfilDokterServices _profilDokterServices, IWebHostEnvironment _webHostEnvironment)
        {
            profilDokterServices = _profilDokterServices;
            webHostEnvironment = _webHostEnvironment;
        }

        //public async Task<JsonResult> GetDataById()
        //{
        //    List<VMTblProfilDokter> data = await profilDokterServices.GetDataById();
        //    return Json(data);
        //}
        public async Task<IActionResult> Index(int id)
        {
            VMTblProfilDokter data = await profilDokterServices.GetDataById(id);
            return View(data);
        }

        public string Upload(IFormFile ImageFile)
        {
            string uniqueFileName = "";

            if (ImageFile != null)
            {
                string uploadFolder = Path.Combine(webHostEnvironment.WebRootPath, "images");
                uniqueFileName = Guid.NewGuid().ToString() + "_" + ImageFile.FileName;
                string filePath = Path.Combine(uploadFolder, uniqueFileName);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    ImageFile.CopyTo(fileStream);
                }
            }

            return uniqueFileName;
        }

        [HttpPost]
        public async Task<IActionResult> SaveImg(VMFoto dataParam)
        {
            if (dataParam.ImageFile != null)
            {
                dataParam.ImagePath = Upload(dataParam.ImageFile);
            }
            //dataParam.CreateBy = IdUser;
            VMResponse respon = await profilDokterServices.SaveImg(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

    }

}
