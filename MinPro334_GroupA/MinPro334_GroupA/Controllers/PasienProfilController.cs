﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;
using System.Net.Mail;
using System.Net;

namespace MinPro334_GroupA.Controllers
{
    public class PasienProfilController : Controller
    {
        private PasienProfilService pasienProfilService;

        public PasienProfilController(PasienProfilService _pasienProfilService)
        {
            pasienProfilService = _pasienProfilService;
        }

        public async Task<IActionResult> Index(VMPasienProfil dataParam)
        {
            dataParam.Id = (long)HttpContext.Session.GetInt32("IdUser");
            VMPasienProfil data = await pasienProfilService.GetAllData(dataParam);


            return View(data);
        }

        public async Task<IActionResult> dataPribadi(VMPasienProfil dataParam)
        {
            dataParam.Id = (long)HttpContext.Session.GetInt32("IdUser");

            VMPasienProfil data = await pasienProfilService.GetAllData(dataParam);

            return PartialView(data);
        }


        [HttpPost]
        public async Task<IActionResult> Edit(VMPasienProfil dataParam)
        {
            dataParam.ModifiedBy = (long)HttpContext.Session.GetInt32("IdUser");
            VMResponse respon = await pasienProfilService.EditCustomer(dataParam);
            VMResponse respon2 = await pasienProfilService.EditBiodata(dataParam);

            if (respon.Success && respon2.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Password(VMPasienProfil dataParam)
        {
            dataParam.Id = (long)HttpContext.Session.GetInt32("IdUser");

            VMPasienProfil data = await pasienProfilService.GetAllData(dataParam);

            return PartialView(data);
        }

        public async Task<JsonResult> CheckPassword(string password, int id)
        {
            bool isExist = await pasienProfilService.CheckPassword(password, id);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> EditPassword(VMPasienProfil dataParam)
        {
            dataParam.ModifiedBy = (long)HttpContext.Session.GetInt32("IdUser");
            VMResponse respon = await pasienProfilService.EditUser(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Email(VMPasienProfil dataParam)
        {
            dataParam.Id = (long)HttpContext.Session.GetInt32("IdUser");

            VMPasienProfil data = await pasienProfilService.GetAllData(dataParam);
            return PartialView(data);
        }

        public async Task<JsonResult> CheckEmail(string email, int id)
        {
            bool isExist = await pasienProfilService.CheckEmail(email, id);
            return Json(isExist);
        }

        public string GetOtp()
        {
            // string number = "0123456789";
            Random random = new Random();
            string otp = string.Empty;
            for (int i = 0; i < 6; i++)
            {
                int addrandom = random.Next(0, 9);
                otp += addrandom;
            }
            return otp;
        }

        [HttpPost]
        public async Task<IActionResult> GenerateOTP(VMResetEmail dataParam)
        {
            var kodeOTP = GetOtp();
            dataParam.Token = kodeOTP;
            dataParam.UsedFor = "lupa Password";

            var fromMail = new MailAddress("mastergozi123@gmail.com", "Admin");
            var fromEmailpassword = "kaxr nmga pxtk dxsa";
            var toMail = new MailAddress(dataParam.Email);

            var smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(fromMail.Address, fromEmailpassword);

            var Message = new MailMessage(fromMail, toMail);
            Message.Subject = "Edit Email";
            Message.Body = "<br/> OTP : " + kodeOTP;
            Message.IsBodyHtml = true;

            smtp.Send(Message);
            dataParam.ModifiedBy = (long)HttpContext.Session.GetInt32("IdUser");
            VMResponse respon = await pasienProfilService.GenerateToken(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return PartialView(dataParam);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateOtp(TToken dataParam)
        {

            dataParam.ModifiedBy = (long)HttpContext.Session.GetInt32("IdUser");
            VMResponse respon = await pasienProfilService.UpdateOtp(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public IActionResult ConfirmOtp(string email, int id)
        {
            ViewBag.Email = email;
            ViewBag.Id = id;
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> EditEmail(VMResetEmail dataParam)
        {
            dataParam.ModifiedBy = (long)HttpContext.Session.GetInt32("IdUser");
            VMResponse respon = await pasienProfilService.EditUserEmail(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<JsonResult> CheckTokenIsExist(string email, string otp)
        {
            bool isExist = await pasienProfilService.CheckByEmailToken(email, otp);
            return Json(isExist);
        }

        public async Task<JsonResult> CheckTokenExp(string email, string otp)
        {
            bool isExist = await pasienProfilService.CheckTokenExp(email, otp);
            return Json(isExist);
        }

    }
}
