﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class HakAksesController : Controller
    {
        private HakAksesService hakAksesService;
        private int IdUser = 1; //

        public HakAksesController (HakAksesService _hakAksesService)
        {
            hakAksesService = _hakAksesService;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.currentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<MRole> data = await hakAksesService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name!.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }

            return View(PaginatedList<MRole>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            MRole data = new MRole();
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(MRole dataParam)
        {
            dataParam.CreatedBy = (long)HttpContext.Session.GetInt32("IdUser");
            VMResponse respon = await hakAksesService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<JsonResult> CheckNameIsExsist(string name, int id)
        {
            bool isExistName = await hakAksesService.CheckName(name, id);
            return Json(isExistName);
        }

        public async Task<JsonResult> CheckCodeIsExsist(string code, int id)
        {
            bool isExistCode = await hakAksesService.CheckCode(code, id);
            return Json(isExistCode);
        }

        public async Task<IActionResult> Edit(int id)
        {
            MRole data = await hakAksesService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(MRole dataParam)
        {
            dataParam.ModifiedBy = IdUser;
            VMResponse respon = await hakAksesService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            MRole data = await hakAksesService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            int deletedBy = IdUser;
            VMResponse respon = await hakAksesService.Delete(id, deletedBy);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("Index");
        }

    }
}
