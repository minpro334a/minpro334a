﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;
using System.Net.Mail;
using System.Net;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Controllers
{
    public class AuthController : Controller
    {
        private AuthService authService;
        private int IdUser = 0;
        VMResponse respon = new VMResponse();
        public AuthController(AuthService _authService)
        {
            authService = _authService;

        }
        public IActionResult Login()
        {
            return PartialView();
        }
        [HttpPost]
        public async Task<JsonResult> LoginSubmit(string email, string password)
        {
            VMResponse response = await authService.CheckLogin(email, password);

            if (!response.Success)
            {
                respon.Success = false;
                respon.Message = response.Message;
                return Json(new { dataRespon = respon });
            }

            VMMUser user = JsonConvert.DeserializeObject<VMMUser>(response.Entity.ToString());

            respon.Message = $"Hello, Welcome to Med.Id";
            HttpContext.Session.SetString("FullName", user.FullName);
            HttpContext.Session.SetInt32("IdUser", (int)user.Id);
            HttpContext.Session.SetInt32("IdRole", (int)user.RoleId);
            HttpContext.Session.SetString("ImagePath", user.ImagePath);

            return Json(new { dataRespon = respon });
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Register()
        {
            return PartialView();
        }

        public async Task<JsonResult> CheckEmailIsExist(string email)
        {
            bool isExist = await authService.CheckByEmail(email);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> GenerateOTP(VMTToken dataParam)
        {
            var kodeOTP = CreateOTP();

            //Send Kode OTP via Email
            var fromMail = new MailAddress("mydokterpocket@gmail.com", "Pockey");
            var fromEmailpassword = "eatn wexl elhy tubp";
            var toMail = new MailAddress(dataParam.Email);

            var smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential(fromMail.Address, fromEmailpassword);

            var Message = new MailMessage(fromMail, toMail);
            Message.Subject = "Initial Login";
            Message.Body = "<br/> OTP : " + kodeOTP;
            Message.IsBodyHtml = true;

            smtp.Send(Message);

            dataParam.Token = kodeOTP;


            VMResponse respon = await authService.GenerateToken(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return PartialView(dataParam);
        }

        public string CreateOTP()
        {
            string OTPLength = "6";
            string OTP = string.Empty;

            string Chars = string.Empty;
            Chars = "1,2,3,4,5,6,7,8,9,0";

            char[] seplitChar = { ',' };
            string[] arr = Chars.Split(seplitChar);
            string NewOTP = "";
            string temp = "";
            Random rand = new Random();
            for (int i = 0; i < Convert.ToInt32(OTPLength); i++)
            {
                temp = arr[rand.Next(0, arr.Length)];
                NewOTP += temp;
                OTP = NewOTP;
            }
            return OTP;
        }

        public async Task<IActionResult> KonfirmasiOTP(string email)
        {
            ViewBag.Email = email;
            return PartialView();
        }
        public async Task<IActionResult> KonfirmasiOTPLupaPassword(string email)
        {
            ViewBag.Email = email;
            return PartialView();
        }

        public async Task<JsonResult> CheckTokenIsExpired(string email, string token)
        {
            VMResponse respon = await authService.CheckIsExpired(email, token);

            //bool isvalid = await daftarService.CheckIsExpired(email, token);
            //return Json(isvalid);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return Json(new { dataRespon = respon });
        }


        public async Task<JsonResult> CheckTokenByEmail(string email, string token)
        {
            bool isvalid = await authService.CheckTokenByEmail(email, token);
            return Json(isvalid);
        }
        public async Task<IActionResult> SetPassword(string email)
        {
            ViewBag.Email = email;
            return PartialView();
        }

        public async Task<IActionResult> SetPasswordLupaPassword(string email)
        {
            ViewBag.Email = email;
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> SetPasswordLupaPassword(VMMUser dataParam)
        {
            dataParam.Id = dataParam.Id == null ? IdUser : dataParam.Id;
            dataParam.FullName = dataParam.FullName ?? "";
            dataParam.MobilePhone = dataParam.MobilePhone ?? "";
            dataParam.CreatedBy = dataParam.CreatedBy == null ? 0 : dataParam.CreatedBy;
            dataParam.CreatedOn = dataParam.CreatedOn == null ? DateTime.Now : dataParam.CreatedOn;
            dataParam.IsDelete = dataParam.IsDelete == null ? false : dataParam.IsDelete;

            VMResponse respon = await authService.SetNewPassword(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return PartialView(dataParam);
        }
        public async Task<IActionResult> SignUp(string email, string password)
        {
            ViewBag.Email = email;
            ViewBag.Password = password;

            List<MRole> listRole = await authService.GetAllDataRole();
            ViewBag.listRole = listRole;
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> CreateAccount(VMMUser dataParam) // NO VIEW
        {
            VMResponse respon = await authService.CreateAccount(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public IActionResult LupaPassword()
        {
            return PartialView();
        }








    }
}
