﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Controllers
{
    public class AturAksesMenuController : Controller
    {
        private AturAksesMenuService aturAksesMenuService;
        private int IdUser = 1; //

        public AturAksesMenuController(AturAksesMenuService _aturAksesMenuService)
        {
            aturAksesMenuService = _aturAksesMenuService;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.currentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<MRole> data = await aturAksesMenuService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name!.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }

            return View(PaginatedList<MRole>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMMRole data = await aturAksesMenuService.GetDataById(id);
            ViewBag.role_menu = data.role_menu;
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMMRole dataParam)
        {
            dataParam.ModifiedBy = IdUser;
            VMResponse respon = await aturAksesMenuService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }


    }
}
