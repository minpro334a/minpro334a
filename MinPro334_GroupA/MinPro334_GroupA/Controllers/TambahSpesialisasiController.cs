﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class TambahSpesialisasiController : Controller
    {
        private TambahSpesialisasiService tambahSpesialisasiService;
        private SpecializationService specializationService;
  
        public TambahSpesialisasiController(TambahSpesialisasiService _tambahSpesialisasiService, SpecializationService _specializationService)
        {
            tambahSpesialisasiService = _tambahSpesialisasiService;
            specializationService = _specializationService;
        }

        public async Task<IActionResult> Index(VMTambahSpesialisasi dataparam)
        {
            dataparam.Id = (long)HttpContext.Session.GetInt32("IdUser");
            List<VMTambahSpesialisasi> data = await tambahSpesialisasiService.GetAllData(dataparam);
            return View(data);

        }

        public async Task<IActionResult> Create(int id)
        {
            VMTambahSpesialisasi data = await tambahSpesialisasiService.GetDataById(id);

            List<MSpecialization> listCategory = await specializationService.GetAllData();
            ViewBag.ListCategory = listCategory;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMTambahSpesialisasi dataParam)
        {
            dataParam.Id = (long)HttpContext.Session.GetInt32("IdUser");
            dataParam.CreatedBy = (long)HttpContext.Session.GetInt32("IdUser");
            VMResponse respon = await tambahSpesialisasiService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMTambahSpesialisasi data = await tambahSpesialisasiService.GetDataById(id);

            List<MSpecialization> listCategory = await specializationService.GetAllData();
            ViewBag.ListCategory = listCategory;

            return PartialView(data);

        }
        [HttpPost]
        public async Task<IActionResult> Edit(VMTambahSpesialisasi dataParam)
        {
            dataParam.CreatedBy = (long)HttpContext.Session.GetInt32("IdUser");
            VMResponse respon = await tambahSpesialisasiService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

    }
}
