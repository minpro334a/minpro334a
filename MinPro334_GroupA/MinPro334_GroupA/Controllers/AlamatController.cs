﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class AlamatController : Controller
    {
        private AlamatService alamatService;
        private LokasiService lokasiService;
        private int IdUser = 1; //

        public AlamatController(AlamatService _alamatService, LokasiService _lokasiService)
        {
            alamatService = _alamatService;
            lokasiService = _lokasiService;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize, string dropDownLabel, string curentdropDownLabel)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.currentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (dropDownLabel != null)
            {
                curentdropDownLabel = dropDownLabel;

            }

            ViewBag.CurentdropDownLabel = dropDownLabel;


            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMLocation> data = await alamatService.GetAllData();

            //List<string> label = new List<string>();

            //foreach (var item in data)
            //{
            //    label.Add(item.Label);
            //}

            //ViewBag.listLabel = label.Distinct();


            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Label!.ToLower().Contains(searchString.ToLower())).ToList();
            }
            //if (dropDownLabel != null)
            //{
            //    data = data.Where(a => a.Label == dropDownLabel).ToList();
            //}

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Label).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Label).ToList();
                    break;
            }

            return View(PaginatedList<VMLocation>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public async Task<IActionResult> Create()
        {
            VMLocation data = new VMLocation();

            List<VMLocation> listLocation = await lokasiService.GetAllDataKecKota();
            ViewBag.listLocation = listLocation;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMLocation dataParam)
        {
            VMLocation data = await alamatService.GetBiodataId(IdUser);
            dataParam.BiodataId = data.BiodataId;

            dataParam.CreatedBy = IdUser;
            VMResponse respon = await alamatService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("index");
        }

        public async Task<IActionResult> edit(int id)
        {
            VMLocation data = await alamatService.GetDataById(id);

            List<VMLocation> listLocation = await lokasiService.GetAllDataKecKota();
            ViewBag.listLocation = listLocation;
           //ViewBag.listLocation = new SelectList(listLocation, "LocationId", "NameLocation", data.LocationId);
            
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMLocation dataParam)
        {
            dataParam.CreatedBy = IdUser;
            VMResponse respon = await alamatService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMLocation data = await alamatService.GetDataById(id);

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(long id)
        {
            VMResponse respon = await alamatService.Delete(id);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> MultipleDelete(List<int> listId)
        {
            List<string> listName = new List<string>();
            foreach (var item in listId)
            {
                VMLocation data = await alamatService.GetDataById(item);
                listName.Add(data.Label );
            }

            ViewBag.ListName = listName;
            return PartialView();
        }


        [HttpPost]
        public async Task<IActionResult> SureMultipleDelete(List<int> listId)
        {
            VMResponse respon = await alamatService.MultipleDelete(listId);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }

    }
}
