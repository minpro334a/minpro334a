﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class DetailDokterController : Controller
    {
        private ProfilDokterServices profilDokterServices;
        private readonly IWebHostEnvironment webHostEnvironment;
        private int IdUser = 1;
        public DetailDokterController(ProfilDokterServices _profilDokterServices, IWebHostEnvironment _webHostEnvironment)
        {
            profilDokterServices = _profilDokterServices;
            webHostEnvironment = _webHostEnvironment;
        }

        public async Task<IActionResult> Index(int id)
        {
            VMTblProfilDokter data = await profilDokterServices.GetDataById(id);
            return View(data);
        }

        public async Task<IActionResult> ProfilDokter(int id)
        {
            VMTblProfilDokter data = await profilDokterServices.GetDataById(id);
            return View(data);
        }

        public async Task<IActionResult> CariDokter()
        {
            List<VMCariDokter> data = await profilDokterServices.GetAllDataDokter();
            return View(data);
        }

        public IActionResult Chat()
        {
            return PartialView();
        }
        public IActionResult Janji()
        {
            return PartialView();
        }
    }
}
