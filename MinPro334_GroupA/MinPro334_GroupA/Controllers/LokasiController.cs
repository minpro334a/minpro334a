﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;
using System.Collections.Generic;
using System.Drawing.Text;

namespace MinPro334_GroupA.Controllers
{
    public class LokasiController : Controller
    {
        private LokasiServices lokasiServices;
        private LevelServices levelServices;
        private readonly IWebHostEnvironment webHostEnvironment;
        private int IdUser = 1;

        public LokasiController(LokasiServices _lokasiServices, IWebHostEnvironment _webHostEnvironment, LevelServices _levelServices)
        {
            lokasiServices = _lokasiServices;
            levelServices = _levelServices;
            webHostEnvironment = _webHostEnvironment;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize, int id, long? LevelLocation, long? currentIdLocationLevel)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.currentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";
            ViewBag.NameLocationSort = string.IsNullOrEmpty(sortOrder) ? "namelocation_desc" : "";


            if (LevelLocation == null)
            {
                LevelLocation = currentIdLocationLevel;

            }

            ViewBag.CurrentIdLocationLevel = LevelLocation;

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;


            List<VMTblLokasi> data = await lokasiServices.GetAllData();

            MLocationLevel dataLocation = new MLocationLevel();
            List<MLocationLevel> listLocationLevel = await levelServices.GetAllData();
            ViewBag.listLocationLevel = listLocationLevel;

            if (LevelLocation != null)
            {
                data = data.Where(a => a.LocationLevelId == LevelLocation).ToList();
            }

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())).ToList();
/*                data = data.Where(a => a.NameLocation.ToLower().Contains(searchString.ToLower())).ToList();
*/
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;

                case "namelocation_desc":
                    data = data.OrderByDescending(a => a.NameLocation).ToList();
                    break;

                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    data = data.OrderBy(a => a.NameLocation).ToList();
                    break;
            }

            return View(PaginatedList<VMTblLokasi>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 5));
        }

        public async Task<IActionResult> Create()
        {
            VMTblLokasi data = new VMTblLokasi();

            List<MLocationLevel> ListLevel = await levelServices.GetAllData();
            ViewBag.ListLevel = ListLevel;

            List<VMTblLokasi> ListLocation = await lokasiServices.GetAllData();
            ViewBag.ListLocation = ListLocation;

            return PartialView(data);
        }

        public async Task<JsonResult> GetDataByIdLevel(int id)
        {
            List<VMTblLokasi> data = await lokasiServices.GetDataByIdLevel(id);
            return Json(data);

        }

        public async Task<JsonResult> CheckNameIsExist(int locationLevelId, string name, int id, long parentId)
        {
            bool isExist = await lokasiServices.CheckNameIsExist(locationLevelId, name, id, parentId);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMTblLokasi dataParam)
        {

            VMResponse respon = await lokasiServices.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }


        //[HttpPost]
        //public async Task<IActionResult> Create(VMTblLokasi dataParam)
        //{
        //    dataParam.CreatedBy = IdUser;
        //    dataParam.CreatedOn = DateTime.Now;
        //    VMResponse respon = await lokasiServices.Create(dataParam);

        //    if (respon.Success)
        //    {
        //        return Json(new { dataRespon = respon });
        //    }

        //    return View(dataParam);
        //}

        //public async Task<JsonResult> GetNameParent(int id)
        //{
        //    List<VMTblLokasi> data = await lokasiServices.GetNameParent(id);
        //    return Json(data);
        //}

        public async Task<IActionResult> Edit(int id)
        {
            VMTblLokasi data = await lokasiServices.GetDataById(id);

            List<MLocationLevel> ListLevel = await levelServices.GetAllData();
            ViewBag.ListLevel = ListLevel;
            long? level = data.LocationLevelId;
            List<VMTblLokasi> ListLocation = await lokasiServices.GetAllDataLocationEdit(level);
            ViewBag.ListLocation = ListLocation;


            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMTblLokasi dataParam)
        {
            VMResponse respon = await lokasiServices.Edit(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }



        //public async Task<IActionResult> Edit(int id)
        //{
        //    VMTblLokasi data = await lokasiServices.GetDataById(id);

        //    List<VMTblLokasi> ListLevel = await lokasiServices.GetAllData();
        //    ViewBag.ListLevel = ListLevel;

        //    return PartialView(data);
        //}

        //[HttpPost]
        //public async Task<IActionResult> Edit(VMTblLokasi dataParam)
        //{
        //    dataParam.CreatedBy = IdUser;
        //    VMResponse respon = await lokasiServices.Edit(dataParam);

        //    if (respon.Success)
        //    {
        //        return Json(new { dataRespon = respon });
        //    }

        //    return View(dataParam);
        //}

        public async Task<IActionResult> Delete(int id)
        {
            VMTblLokasi data = await lokasiServices.GetDataById(id);
            return PartialView(data);
        }

        public async Task<JsonResult> CheckWilayahInUserOrNot(long id)
        {
            bool isExist = await lokasiServices.CheckWilayahDigunakan(id);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await lokasiServices.Delete(id);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }


    }
}
