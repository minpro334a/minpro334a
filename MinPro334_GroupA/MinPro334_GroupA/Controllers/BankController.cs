﻿using Microsoft.AspNetCore.Mvc;

using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class BankController : Controller
    {
        private BankService bankService;
       


        public BankController(BankService _bankService)
        {
            bankService = _bankService;
            //int IdUsar = HttpContext.Session.GetInt32("IdUser") ?? 0;
        }


        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<MBank> data = await bankService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }
            return View(PaginatedList<MBank>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 5));
        }

        public async Task<IActionResult> Create()
        {
            return PartialView();
        }

        public async Task<JsonResult> CheckBankIsExist(string name, long id)
        {
            bool isExist = await bankService.CheckBankByName(name, id);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMMBank dataParam)
        {
            dataParam.CreatedBy = (long)HttpContext.Session.GetInt32("IdUser");
            VMResponse respon = await bankService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(long id)
        {

            VMMBank data = await bankService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMMBank dataParam)
        {
            dataParam.ModifiedBy = (long)HttpContext.Session.GetInt32("IdUser");

            VMResponse respon = await bankService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMMBank data = await bankService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(VMMBank dataParam)
        {
            dataParam.DeletedBy = (long)HttpContext.Session.GetInt32("IdUser");

            VMResponse respon = await bankService.Delete(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<JsonResult> CheckByName(string name)
        {
            bool isExist = await bankService.CheckByName(name);
            return Json(isExist);
        }

        public async Task<JsonResult> CheckByCode(string vacode)
        {
            bool isExist = await bankService.CheckByCode(vacode);
            return Json(isExist);
        }

        public async Task<JsonResult> CheckNameCodeIsExist(string name, string vacode)
        {
            bool isExist = await bankService.CheckNameCode(name, vacode);
            return Json(isExist);
        }





    }
}
