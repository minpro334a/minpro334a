﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class CaraPembayaranController : Controller
    {
        private CaraPembayaranService caraPembayaranService;


        public CaraPembayaranController(CaraPembayaranService _caraPembayaranService)
        {
            caraPembayaranService = _caraPembayaranService;
        }
        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<MPaymentMethod> data = await caraPembayaranService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }
            return View(PaginatedList<MPaymentMethod>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 5));
        }

        public async Task<IActionResult> Create()
        {
            return PartialView();
        }

        public async Task<JsonResult> CheckNameIsExist(string name)
        {
            bool isExist = await caraPembayaranService.CheckPaymentMethodByName(name);
            return Json(isExist);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMMPaymentMethod dataParam)
        {
            dataParam.CreatedBy = (long)HttpContext.Session.GetInt32("IdUser");

            VMResponse respon = await caraPembayaranService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Edit(long id)
        {

            VMMPaymentMethod data = await caraPembayaranService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMMPaymentMethod dataParam)
        {
            dataParam.ModifiedBy = (long)HttpContext.Session.GetInt32("IdUser");

            VMResponse respon = await caraPembayaranService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMMPaymentMethod data = await caraPembayaranService.GetDataById(id);
            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(VMMPaymentMethod dataParam)
        {
            dataParam.DeletedBy = (long)HttpContext.Session.GetInt32("IdUser");

            VMResponse respon = await caraPembayaranService.Delete(dataParam);
            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


    }
}
