﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.Controllers
{
    public class PasienController : Controller
    {
        private PasienService pasienService;
        private GolonganDarahService golonganDarahService;
        private HubunganPasienService hubunganPasienService;
        private int IdUser = 1; //

        public PasienController(PasienService _pasienService, GolonganDarahService _golonganDarahService, HubunganPasienService _hubunganPasienService)
        {
            pasienService = _pasienService;
            golonganDarahService = _golonganDarahService;
            hubunganPasienService = _hubunganPasienService;
        }

        public async Task<IActionResult> Index(string sortOrder, string searchString, string currentFilter, int? pageNumber, int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            List<VMCustomerMember> data = await pasienService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Fullname.ToLower().Contains(searchString.ToLower())).ToList();
            }

            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Fullname).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Fullname).ToList();
                    break;
            }

            return View(PaginatedList<VMCustomerMember>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));

        }

        public async Task<IActionResult> Create()
        {
            VMCustomerMember data = new VMCustomerMember();

            List<MBloodGroup> listBloodGroup = await golonganDarahService.GetAllData();
            ViewBag.ListBloodGroup = listBloodGroup;

            List<MCustomerRelation> listRelation = await hubunganPasienService.GetAllData();
            ViewBag.ListRelation = listRelation;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Create(VMCustomerMember dataParam)
        {
            dataParam.CreatedBy = IdUser;
            VMResponse respon = await pasienService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("index");
        }

        public async Task<IActionResult> Edit(int id)
        {
            VMCustomerMember data = await pasienService.GetDataById(id);

            List<MBloodGroup> listBloodGroup = await golonganDarahService.GetAllData();
            ViewBag.ListBloodGroup = listBloodGroup;

            List<MCustomerRelation> listRelation = await hubunganPasienService.GetAllData();
            ViewBag.ListRelation = listRelation;

            return PartialView(data);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(VMCustomerMember dataParam)
        {
            dataParam.CreatedBy = IdUser;
            VMResponse respon = await pasienService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Delete(int id)
        {
            VMCustomerMember data = await pasienService.GetDataById(id);

            return PartialView(data);

        }

        [HttpPost]
        public async Task<IActionResult> SureDelete(int id)
        {
            VMResponse respon = await pasienService.Delete(id);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> MultipleDelete(List<int> listId)
        {
            List<string> listName = new List<string>();
            foreach (int item in listId)
            {
                VMCustomerMember data = await pasienService.GetDataById(item);
                listName.Add(data.Fullname);
            }

            ViewBag.ListName = listName;
            return PartialView();
        }

        [HttpPost]
        public async Task<IActionResult> SureMultipleDelete(List<int> listId)
        {
            VMResponse respon = await pasienService.MultipleDelete(listId);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return RedirectToAction("Index");
        }
    }
}
