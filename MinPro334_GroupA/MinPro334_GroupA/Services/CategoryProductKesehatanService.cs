﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class CategoryProductKesehatanService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public CategoryProductKesehatanService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MMedicalItemCategory>> GetAllData()
        {
            List<MMedicalItemCategory> data = new List<MMedicalItemCategory>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCategoryProductKesehatan/GetAllData");
            data = JsonConvert.DeserializeObject<List<MMedicalItemCategory>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Create(MMedicalItemCategory dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PostAsync(RouteAPI + "apiCategoryProductKesehatan/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();

                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<bool> CheckCategoryByName(string name, int id)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiCategoryProductKesehatan/CheckCategoryByName/{name}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExist;
        }

        public async Task<MMedicalItemCategory> GetDataById(int id)
        {
            MMedicalItemCategory data = new MMedicalItemCategory();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCategoryProductKesehatan/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<MMedicalItemCategory>(apiResponse)!;
            return data;
        }

        public async Task<VMResponse> Edit(MMedicalItemCategory dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PutAsync(RouteAPI + "apiCategoryProductKesehatan/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();

                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Delete(MMedicalItemCategory dataParam)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCategoryProductKesehatan/Delete/{dataParam.Id}/{dataParam.DeletedBy}");

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
