﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Services
{
    public class ObatService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public ObatService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMMedicalItemPurchaseDetail>> GetAllData()
        {
            List<VMMedicalItemPurchaseDetail> data = new List<VMMedicalItemPurchaseDetail>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiObat/GetAllDataOrderHeader");
            data = JsonConvert.DeserializeObject<List<VMMedicalItemPurchaseDetail>>(apiResponse)!;

            return data;
        }

    }
}
