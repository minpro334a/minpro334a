﻿using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Services
{
    public class CariDokterServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public CariDokterServices(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }


        public async Task<List<VMTblProfilDokter>> GetAllData()
        {
            List<VMTblProfilDokter> data = new List<VMTblProfilDokter>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiProfilDokter/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMTblProfilDokter>>(apiResponse)!;

            return data;
        }
        public async Task<VMTblProfilDokter> GetDataById(int id)
        {
            VMTblProfilDokter data = new VMTblProfilDokter();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"Profil/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblProfilDokter>(apiResponse)!;

            return data;


        }
    }
}
