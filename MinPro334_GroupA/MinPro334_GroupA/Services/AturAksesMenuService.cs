﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class AturAksesMenuService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public AturAksesMenuService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MRole>> GetAllData()
        {
            List<MRole> data = new List<MRole>();

            string apiRespon = await client.GetStringAsync(RouteAPI + "apiHakAkses/GetAllData/"); //
            data = JsonConvert.DeserializeObject<List<MRole>>(apiRespon);

            return data;
        }

        public async Task<VMMRole> GetDataById(int id)
        {
            VMMRole data = new VMMRole();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAturAksesMenu/GetDataById/{id}"); //get url API
            data = JsonConvert.DeserializeObject<VMMRole>(apiResponse); //deserialis mengubah json menjadi objek, serialis sebaliknya
            return data;
        }

        public async Task<VMResponse> Edit(VMMRole dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiAturAksesMenu/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }
    }
}
