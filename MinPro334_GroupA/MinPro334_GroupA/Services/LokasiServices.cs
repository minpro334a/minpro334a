﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class LokasiServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public LokasiServices(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMTblLokasi>> GetAllData()
        {
            List<VMTblLokasi> data = new List<VMTblLokasi>();

            //menjdapatkan api 
            string apiRespon = await client.GetStringAsync(RouteAPI + "apiLokasi/GetAllData");
            //mengubah string menjadi object list 
            data = JsonConvert.DeserializeObject<List<VMTblLokasi>>(apiRespon);
            return data;
        }

        public async Task<List<VMTblLokasi>> GetDataByIdLevel(long id)
        {
            List<VMTblLokasi> data = new List<VMTblLokasi>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiLokasi/GetDataByIdLevel/{id}");
            data = JsonConvert.DeserializeObject<List<VMTblLokasi>>(apiResponse);
            return data;
        }

        public async Task<List<VMTblLokasi>> GetAllDataLocationEdit(long? level)
        {
            List<VMTblLokasi> data = new List<VMTblLokasi>();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiLokasi/GetAllDataLocationEdit/{level}");
            data = JsonConvert.DeserializeObject<List<VMTblLokasi>>(apiResponse);
            return data;
        }

        public async Task<bool> CheckNameIsExist(long locationLevelId, string name, long id, long parentId)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiLokasi/CheckNameIsExist/{locationLevelId}/{name}/{id}/{parentId}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isExist;
        }

        public async Task<VMResponse> Create(VMTblLokasi dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu kirim ke request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirim Body
            var request = await client.PostAsync(RouteAPI + "apiLokasi/Save", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke Objeect
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMTblLokasi> GetDataById(long id)
        {
            VMTblLokasi data = new VMTblLokasi();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiLokasi/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblLokasi>(apiResponse);
            return data;
        }

        public async Task<VMResponse> Edit(VMTblLokasi dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu kirim ke request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil API dan mengirim Body
            var request = await client.PutAsync(RouteAPI + "apiLokasi/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke Objeect
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<bool> CheckWilayahDigunakan(long id)
        {
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiLokasi/CheckWilayahDigunakan/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiResponse);
            return isExist;
        }


        public async Task<VMResponse> Delete(long id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiLokasi/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }


        //public async Task<List<VMTblLokasi>> GetAllData()
        //{
        //    List<VMTblLokasi> data = new List<VMTblLokasi>();

        //    string apiResponse = await client.GetStringAsync(RouteAPI + "apiLokasi/GetAllData");
        //    data = JsonConvert.DeserializeObject<List<VMTblLokasi>>(apiResponse)!;

        //    return data;
        //}

        //public async Task<VMTblLokasi> GetDataById(int id)
        //{
        //    VMTblLokasi data = new VMTblLokasi();

        //    string apiResponse = await client.GetStringAsync(RouteAPI + $"apiLokasi/GetDataById/{id}");
        //    data = JsonConvert.DeserializeObject<VMTblLokasi>(apiResponse)!;

        //    return data;
        //}

        //public async Task<VMTblLokasi> GetNameParent()
        //{
        //    VMTblLokasi data = new VMTblLokasi();

        //    string apiResponse = await client.GetStringAsync(RouteAPI + $"apiLokasi/GetNameParent");
        //    data = JsonConvert.DeserializeObject<VMTblLokasi>(apiResponse)!;

        //    return data;
        //}

        //public async Task<List<VMTblLokasi>> GetDataByIdLevel(int id)
        //{
        //    id = id + 1;
        //    List<VMTblLokasi> data = new List<VMTblLokasi>();

        //    string apiResponse = await client.GetStringAsync(RouteAPI + $"apiLokasi/GetDataByIdLevel/{id}");
        //    data = JsonConvert.DeserializeObject<List<VMTblLokasi>>(apiResponse)!;

        //    return data;
        //}

        //public async Task<List<VMTblLokasi>> GetNameParent(int id)
        //{
        //    List<VMTblLokasi> data = new List<VMTblLokasi>();
        //    string apiResponse = await client.GetStringAsync(RouteAPI + $"apiLokasi/GetNameParent/{id}");
        //    data = JsonConvert.DeserializeObject<List<VMTblLokasi>>(apiResponse)!;
        //    return data;
        //}

        //public async Task<VMResponse> Create(VMTblLokasi dataParam)
        //{
        //    //proses convert dari object ke string
        //    string json = JsonConvert.SerializeObject(dataParam);

        //    StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

        //    var request = await client.PostAsync(RouteAPI + "apiLokasi/Save", content);

        //    if (request.IsSuccessStatusCode)
        //    {
        //        var apiRespon = await request.Content.ReadAsStringAsync();

        //        respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
        //    }
        //    else
        //    {
        //        respon.Success = false;
        //        respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
        //    }

        //    return respon;
        //}

        //public async Task<bool> CheckByName(string input,int id, int locationLevelId)
        //{
        //    string apiRespon = await client.GetStringAsync(RouteAPI + $"apiLokasi/CheckByName/{input}/{id}/{locationLevelId}");
        //    bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

        //    return isExist;
        //}

        //public async Task<VMResponse> Edit(VMTblLokasi dataParam)
        //{
        //    //proses convert dari object ke string
        //    string json = JsonConvert.SerializeObject(dataParam);

        //    StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

        //    var request = await client.PutAsync(RouteAPI + "apiLokasi/Edit", content);

        //    if (request.IsSuccessStatusCode)
        //    {
        //        var apiRespon = await request.Content.ReadAsStringAsync();

        //        respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
        //    }
        //    else
        //    {
        //        respon.Success = false;
        //        respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
        //    }

        //    return respon;
        //}

        //public async Task<VMResponse> Delete(int id)
        //{
        //    var request = await client.DeleteAsync(RouteAPI + $"apiLokasi/Delete/{id}");

        //    if (request.IsSuccessStatusCode)
        //    {
        //        //proses membaca respon dari api
        //        var apiRespon = await request.Content.ReadAsStringAsync();

        //        //proses convert hasil respon dari api ke object
        //        respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
        //    }
        //    else
        //    {
        //        respon.Success = false;
        //        respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
        //    }

        //    return respon;
        //}


    }
}
