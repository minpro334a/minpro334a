﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Services
{
    public class DokterService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        private string json;

        public DokterService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMMBiodata>> GetAllData()
        {
            List<VMMBiodata> data = new List<VMMBiodata>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiDokter/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMMBiodata>>(apiResponse);

            return data;
        }

        public async Task<List<VMCariDokter>> CariAllData()
        {
            List<VMCariDokter> data = new List<VMCariDokter>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiDokter/CariAllData");
            data = JsonConvert.DeserializeObject<List<VMCariDokter>>(apiResponse);

            return data;
        }


        public async Task<List<MLocation>> GetAllDataLokasi()
        {
            List<MLocation> data = new List<MLocation>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiDokter/GetAllDataLokasi");
            data = JsonConvert.DeserializeObject<List<MLocation>>(apiResponse);

            return data;
        }

     
        public async Task<List<MSpecialization>> GetAllDataSpecialization()
        {
            List<MSpecialization> data = new List<MSpecialization>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiDokter/GetAllDataSpecialization");
            data = JsonConvert.DeserializeObject<List<MSpecialization>>(apiResponse);

            return data;
        }
        public async Task<List<TDoctorTreatment>> GetAllDataTreatment()
        {
            List<TDoctorTreatment> data = new List<TDoctorTreatment>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiDokter/GetAllDataTreatment");
            data = JsonConvert.DeserializeObject<List<TDoctorTreatment>>(apiResponse);

            return data;
        }

        public async Task<List<MLocationLevel>> GetAllDataLocationLevel()
        {
            List<MLocationLevel> data = new List<MLocationLevel>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiDokter/GetAllDataLocationLevel");
            data = JsonConvert.DeserializeObject<List<MLocationLevel>>(apiResponse);

            return data;
        }





    }
}
