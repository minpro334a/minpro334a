﻿using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class MedicalItemService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public MedicalItemService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMMMedicalItem>> GetAllData()
        {
            List<VMMMedicalItem> data = new List<VMMMedicalItem>();

            string apiRespon = await client.GetStringAsync(RouteAPI + "apiMedicalItem/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMMMedicalItem>>(apiRespon)!;

            return data;
        }

        public async Task<bool> CheckByName(string name, int id, int IdSegmentation, int IdCategory)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiMedicalItem/CheckByName/{name}/{id}/{IdSegmentation}/{IdCategory}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExist;
        }

        public async Task<VMResponse> Create(VMMMedicalItem dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PostAsync(RouteAPI + "apiMedicalItem/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();

                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                var errorContent = await request.Content.ReadAsStringAsync();

                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
                Console.WriteLine($"Isi Konten Kesalahan: {errorContent}");

            }

            return respon;
        }

        public async Task<VMMMedicalItem> GetDataById(int id)
        {
            VMMMedicalItem data = new VMMMedicalItem();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiMedicalItem/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMMMedicalItem>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> Edit(VMMMedicalItem dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PutAsync(RouteAPI + "apiMedicalItem/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();

                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiMedicalItem/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }

    }
}
