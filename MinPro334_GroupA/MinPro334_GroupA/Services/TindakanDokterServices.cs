﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class TindakanDokterServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public TindakanDokterServices(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<VMTblProfilDokter> GetDataByIdForTreatment()
        {
            VMTblProfilDokter data = new VMTblProfilDokter();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"Profil/GetDataByIdForTreatment");
            data = JsonConvert.DeserializeObject<VMTblProfilDokter>(apiResponse)!;

            return data;
        }

        public async Task<TDoctorTreatment> GetDataByIdTreatment( int id)
        {
            TDoctorTreatment data = new TDoctorTreatment();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"Profil/GetDataByIdTreatment/{id}");
            data = JsonConvert.DeserializeObject<TDoctorTreatment>(apiResponse)!;
            return data;
        }

        public async Task<VMResponse> Create(TDoctorTreatment dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PostAsync(RouteAPI + "Profil/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();

                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<bool> CheckTreatmentByName(string name, int id)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"Profil/CheckTreatmentByName/{name}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExist;
        }

        public async Task<VMResponse> Delete(int id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"Profil/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

    }
}
