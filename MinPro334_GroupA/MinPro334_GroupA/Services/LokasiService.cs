﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Services
{
    public class LokasiService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public LokasiService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMLocation>> GetAllDataKecKota()
        {
            List<VMLocation> data = new List<VMLocation>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiLokasi/GetAllDataKecKota");
            data = JsonConvert.DeserializeObject<List<VMLocation>>(apiResponse);

            return data;
        }


    }
}
