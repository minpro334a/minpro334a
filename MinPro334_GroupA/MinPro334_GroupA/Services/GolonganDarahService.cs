﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Services
{
    public class GolonganDarahService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public GolonganDarahService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MBloodGroup>> GetAllData()
        {
            List<MBloodGroup> data = new List<MBloodGroup>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiGolonganDarah/GetAllData");
            data = JsonConvert.DeserializeObject<List<MBloodGroup>>(apiResponse);

            return data;
        }
    }
}
