﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

using Newtonsoft.Json;

using System.Text;

namespace MinPro334_GroupA.Services
{
    public class BankService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        public BankService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MBank>> GetAllData()
        {
            List<MBank> data = new List<MBank>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiBank/GetAllData");
            data = JsonConvert.DeserializeObject<List<MBank>>(apiResponse);
            return data;
        }

        public async Task<bool> CheckBankByName(string name, long id)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiBank/CheckBankByName/{name}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }

        public async Task<VMResponse> Create(VMMBank dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiBank/SaveBank", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMMBank> GetDataById(long id)
        {
            VMMBank data = new VMMBank();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiBank/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMMBank>(apiResponse);
            return data;
        }

        public async Task<VMResponse> Edit(VMMBank dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiBank/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(VMMBank dataParam)
        {

            var request = await client.DeleteAsync(RouteAPI + $"apiBank/Delete/{dataParam.Id}/{dataParam.DeletedBy}");
            //var request = await client.DeleteAsync(RouteAPI + $"apiCaraPembayaran/Delete/{id}");

            if (request.IsSuccessStatusCode)
            {
                // Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

        public async Task<bool> CheckByName(string name)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiBank/CheckByName/{name}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }

        public async Task<bool> CheckByCode(string vacode)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiBank/CheckByCode/{vacode}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }

        public async Task<bool> CheckNameCode(string name, string vacode)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiBank/CheckNameCode/{name}/{vacode}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }


    }
}
