﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class HakAksesService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public HakAksesService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MRole>> GetAllData()
        {
            List<MRole> data = new List<MRole>();

            string apiRespon = await client.GetStringAsync(RouteAPI + "apiHakAkses/GetAllData/");
            data = JsonConvert.DeserializeObject<List<MRole>>(apiRespon);

            return data;
        }

        public async Task<VMResponse> Create(MRole dataParam)
        {
            string json = JsonConvert.SerializeObject(dataParam);

            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            var request = await client.PostAsync(RouteAPI + "apiHakAkses/Save", content);

            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();

                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }

        public async Task<bool> CheckName(string name, int id)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiHakAkses/CheckName/{name}/{id}");
            bool isExistName = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExistName;
        }

        public async Task<bool> CheckCode(string code, int id)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiHakAkses/CheckCode/{code}/{id}");
            bool isExistCode = JsonConvert.DeserializeObject<bool>(apiRespon);

            return isExistCode;
        }

        public async Task<MRole> GetDataById(int id)
        {
            MRole data = new MRole();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiHakAkses/GetDataById/{id}"); //get url API
            data = JsonConvert.DeserializeObject<MRole>(apiResponse); //deserialis mengubah json menjadi objek, serialis sebaliknya
            return data;
        }

        public async Task<VMResponse> Edit(MRole dataParam)
        {
            //Proses convert dari objek ke string format json
            string json = JsonConvert.SerializeObject(dataParam);

            //proses mengubah string menjadi json lalu dikirim sebagai requast bofy
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            //proses memanggil api dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiHakAkses/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari api
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari api ke objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id, int createBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiHakAkses/Delete/{id}/{createBy}");

            if (request.IsSuccessStatusCode)
            {

                // ini adalah proses baca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //proses convert hasil respon dari API ke Objek
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
    }
}
