﻿using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class AlamatService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public AlamatService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<VMLocation>> GetAllData()
        {
            List<VMLocation> data = new List<VMLocation>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiAlamat/GetAllData");
            data = JsonConvert.DeserializeObject<List<VMLocation>>(apiResponse);

            return data;
        }

        public async Task<VMLocation> GetBiodataId(long IdUser)
        {
            VMLocation data = new VMLocation();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAlamat/GetDataById/{IdUser}");
            data = JsonConvert.DeserializeObject<VMLocation>(apiResponse);
            return data;
        }

        public async Task<VMResponse> Create(VMLocation dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiAlamat/Submit", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMLocation> GetDataById(int id)
        {
            VMLocation data = new VMLocation();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAlamat/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMLocation>(apiResponse);

            return data;
        }

        public async Task<VMResponse> Edit(VMLocation dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiAlamat/Edit", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(long id)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiAlamat/Delete/{id}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> MultipleDelete(List<int> listId)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(listId);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiAlamat/MultipleDelete", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

    }
}
