﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class SpecializationService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        private string json;

        public SpecializationService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MSpecialization>> GetAllData()
        {
            List<MSpecialization> data = new List<MSpecialization>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiSpecialization/GetAllData");
            data = JsonConvert.DeserializeObject<List<MSpecialization>>(apiResponse);

            return data;
        }

        public async Task<VMResponse> Create(MSpecialization dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiSpecialization/Save", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<bool> CheckSpecializationByName(string name, int id)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiSpecialization/CheckSpecializationByName/{name}/{id}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apiRespon);
            return isExist;
        }

        public async Task<MSpecialization> GetDataById(int id)
        {
            MSpecialization data = new MSpecialization();
            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiSpecialization/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<MSpecialization>(apiResponse);
            return data;
        }

        public async Task<VMResponse> Edit(MSpecialization dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiSpecialization/Edit", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(int id, int createBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiSpecialization/Delete/{id}/{createBy}");
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;

        }
    }
}
