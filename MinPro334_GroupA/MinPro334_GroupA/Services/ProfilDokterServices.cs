﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class ProfilDokterServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public ProfilDokterServices(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<VMTblProfilDokter> GetDataById(int id)
        {
            VMTblProfilDokter data = new VMTblProfilDokter();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"Profil/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTblProfilDokter>(apiResponse)!;

            return data;

        }

        public async Task<List<VMCariDokter>> GetAllDataDokter()
        {
            List<VMCariDokter> data = new List<VMCariDokter>();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"Profil/GetAllDataDokter");
            data = JsonConvert.DeserializeObject<List<VMCariDokter>>(apiResponse)!;

            return data;
        }

        public async Task<VMResponse> SaveImg(VMFoto data)
        {
            string json = JsonConvert.SerializeObject(data);
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            var request = await client.PostAsync(RouteAPI + "Profil/SaveImg", content);
            if (request.IsSuccessStatusCode)
            {
                var apiRespon = await request.Content.ReadAsStringAsync();
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
    }
}
