﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Services
{
    public class HubunganPasienService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public HubunganPasienService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MCustomerRelation>> GetAllData()
        {
            List<MCustomerRelation> data = new List<MCustomerRelation>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiHubunganPasien/GetAllData");
            data = JsonConvert.DeserializeObject<List<MCustomerRelation>>(apiResponse);

            return data;
        }
    }
}
