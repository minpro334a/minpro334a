﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class CaraPembayaranService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        public CaraPembayaranService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MPaymentMethod>> GetAllData()
        {
            List<MPaymentMethod> data = new List<MPaymentMethod>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiCaraPembayaran/GetAllData");
            data = JsonConvert.DeserializeObject<List<MPaymentMethod>>(apiResponse);
            return data;
        }

        public async Task<bool> CheckPaymentMethodByName(string name)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiCaraPembayaran/CheckPaymentMethodByName/{name}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }

        public async Task<VMResponse> Create(VMMPaymentMethod dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiCaraPembayaran/SaveName", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMMPaymentMethod> GetDataById(long id)
        {
            VMMPaymentMethod data = new VMMPaymentMethod();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiCaraPembayaran/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMMPaymentMethod>(apiResponse);
            return data;
        }

        public async Task<VMResponse> Edit(VMMPaymentMethod dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiCaraPembayaran/Edit", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> Delete(VMMPaymentMethod dataParam)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiCaraPembayaran/Delete/{dataParam.Id}/{dataParam.DeletedBy}");

            if (request.IsSuccessStatusCode)
            {
                // Proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses convert hasil respon dari API ke Object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }

    }
}
