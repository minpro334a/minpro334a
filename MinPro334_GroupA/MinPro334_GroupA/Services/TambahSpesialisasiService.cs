﻿using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class TambahSpesialisasiService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();
        private string json;
        public TambahSpesialisasiService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }
        public async Task<List<VMTambahSpesialisasi>> GetAllData(VMTambahSpesialisasi dataparam)
        {
            List<VMTambahSpesialisasi> data = new List<VMTambahSpesialisasi>();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiTambahSpesialisasi/GetAllData/{dataparam.Id}");
            data = JsonConvert.DeserializeObject<List<VMTambahSpesialisasi>>(apiResponse)!;

            return data;
        }

        public async Task<VMTambahSpesialisasi> GetDataById(int id)
        {
            VMTambahSpesialisasi data = new VMTambahSpesialisasi();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiTambahSpesialisasi/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMTambahSpesialisasi>(apiResponse);

            return data;
        }

        //public async Task<VMResponse> Create(VMTambahSpesialisasi dataParam)
        //{
        //    //proses convert dari object ke string
        //    string json = JsonConvert.SerializeObject(dataParam);
        //    //proses mengubah string menjadi json lalu dikirim sebagai request body
        //    StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
        //    //proses memanggil API dan mengirimkan body
        //    var request = await client.PostAsync(RouteAPI + "apiTambahSpesialisasi/Save", content);
        //    if (request.IsSuccessStatusCode)
        //    {
        //        //proses membaca respon dari API
        //        var apiRespon = await request.Content.ReadAsStringAsync();
        //        //proses convert hasil respon dari API ke object
        //        respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
        //    }
        //    else
        //    {
        //        respon.Success = false;
        //        respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
        //    }
        //    return respon;
        //}

        public async Task<VMResponse> Edit(VMTambahSpesialisasi dataParam)
        {
            //proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            //proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            //proses memanggil API dan mengirimkan body
            var request = await client.PutAsync(RouteAPI + "apiTambahSpesialisasi/Edit", content);
            if (request.IsSuccessStatusCode)
            {
                //proses membaca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();
                //proses convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
    }
}
