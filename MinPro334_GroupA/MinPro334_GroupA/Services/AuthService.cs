﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;
using System.Drawing;
using System.Text;

namespace MinPro334_GroupA.Services
{
    public class AuthService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();


        public AuthService(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<VMResponse> CheckLogin(string email, string password)
        {
            //VMBiodataUser data = new VMBiodataUser();

            string apiRequest = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckLogin/{email}/{password}");

            VMResponse data = JsonConvert.DeserializeObject<VMResponse>(apiRequest);

            return data;
        }
        public async Task<List<VMMMenuAccess>> MenuAccess(int IdRole)
        {
            List<VMMMenuAccess> data = new List<VMMMenuAccess>();

            string apiResponse = await client.GetStringAsync(RouteAPI + $"apiAuth/MenuAccess/{IdRole}");
            data = JsonConvert.DeserializeObject<List<VMMMenuAccess>>(apiResponse);

            return data;
        }

        public async Task<bool> CheckByEmail(string email)
        {
            string apirespon = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckByEmail/{email}");
            bool isExist = JsonConvert.DeserializeObject<bool>(apirespon);
            return isExist;
        }

        public async Task<VMResponse> GenerateToken(VMTToken dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiAuth/SaveToken", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<VMResponse> CheckIsExpired(string email, string token)
        {
            var apiRespon = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckIsExpired/{email}/{token}");
            VMResponse respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            return respon;
        }

        public async Task<bool> CheckTokenByEmail(string email, string token)
        {
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiAuth/CheckTokenByEmail/{email}/{token}");
            bool isvalid = JsonConvert.DeserializeObject<bool>(apiRespon);
            return isvalid;
        }

        public async Task<VMResponse> SetNewPassword(VMMUser dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiAuth/SaveNewPassword", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }

        public async Task<List<MRole>> GetAllDataRole()
        {
            List<MRole> data = new List<MRole>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiAuth/GetAllDataRole");
            data = JsonConvert.DeserializeObject<List<MRole>>(apiResponse);
            return data;
        }

        public async Task<VMResponse> CreateAccount(VMMUser dataParam)
        {
            // convert dari object ke string json
            string json = JsonConvert.SerializeObject(dataParam);

            // mengubah string json lalu dikirim sebagai requst body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");

            // proses memanggil API dan mengirimkan body
            var request = await client.PostAsync(RouteAPI + "apiAuth/SaveCreateAccount", content);

            if (request.IsSuccessStatusCode)
            {
                //membca respon dari API
                var apiRespon = await request.Content.ReadAsStringAsync();

                //convert hasil respon dari API ke object
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon);
            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }


    }
}
