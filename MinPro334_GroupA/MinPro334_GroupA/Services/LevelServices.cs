﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using Newtonsoft.Json;

namespace MinPro334_GroupA.Services
{
    public class LevelServices
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public LevelServices(IConfiguration _configuration)
        {
            configuration = _configuration;
            RouteAPI = configuration["RouteAPI"];
        }

        public async Task<List<MLocationLevel>> GetAllData()
        {
            List<MLocationLevel> data = new List<MLocationLevel>();

            string apiResponse = await client.GetStringAsync(RouteAPI + "apiLevel/GetAllData");
            data = JsonConvert.DeserializeObject<List<MLocationLevel>>(apiResponse)!;

            return data;
        }
    }
}
