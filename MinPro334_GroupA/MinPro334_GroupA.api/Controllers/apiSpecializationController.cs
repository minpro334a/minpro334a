﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiSpecializationController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiSpecializationController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MSpecialization> GetAllData()
        {
            List<MSpecialization> data = db.MSpecializations.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public MSpecialization DataById(int id)
        {
            MSpecialization result = db.MSpecializations.Where(a => a.Id == id).FirstOrDefault();
            return result;
        }

        [HttpGet("CheckSpecializationByName/{name}/{id}")]
        public bool CheckName(string name, int id)
        {
            MSpecialization data = new MSpecialization();
            if (id == 0)//ini untuk saat create
            {
                data = db.MSpecializations.Where(a => a.Name == name && a.IsDelete == false).FirstOrDefault();
            }
            else // untuk edit 
            {
                data = db.MSpecializations.Where(a => a.Name == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }
            if (data != null)
            {
                return true;
            }
            return false;
        }
        [HttpPost("Save")]

        public VMResponse Save(MSpecialization data)
        {
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;
           

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved" + ex.Message;
            }
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(MSpecialization data)
        {
            MSpecialization dt = db.MSpecializations.Where(a => a.Id == data.Id).FirstOrDefault();
            if (dt != null)
            {
                dt.Name = data.Name;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + ex.Message;
                }
                return respon;
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;

        }

        [HttpDelete("Delete/{id}/{createBy}")]

        public VMResponse Delete(int id, int createBy)
        {
            MSpecialization dt = db.MSpecializations.Where(a => a.Id == id).FirstOrDefault();
            if (dt != null)
            {

                dt.IsDelete = true;
                dt.ModifiedBy = createBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted : " + ex.Message;
                }
                return respon;
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;

        }
    }


}
