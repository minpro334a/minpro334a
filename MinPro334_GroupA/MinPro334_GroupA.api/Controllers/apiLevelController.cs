﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiLevelController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;
        public apiLevelController(DB_SpecificationContext _db)
        {
            db = _db;
        }



        [HttpGet("GetAllData")]
        public List<MLocationLevel> GetAllData()
        {
            List<MLocationLevel> data = db.MLocationLevels.Where(a => a.IsDelete == false).ToList();
            return data;
        }
    }
}
