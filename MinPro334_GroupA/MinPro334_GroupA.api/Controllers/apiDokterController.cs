﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiDokterController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiDokterController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        //[HttpGet("GetAllData")]
        //public List<VMMBiodata> GetAllData()
        //{
        //    DateTime now = DateTime.Now;

        //    List<VMMBiodata> data = (from b in db.MBiodata
        //                             join d in db.MDoctors on b.Id equals d.BiodataId
        //                             join cd in db.TCurrentDoctorSpecializations on d.Id equals cd.DoctorId
        //                             join s in db.MSpecializations on cd.SpecializationId equals s.Id
        //                             join dO in db.TDoctorOffices on d.Id equals dO.DoctorId
        //                             join mf in db.MMedicalFacilities on dO.MedicalFacilityId equals mf.Id
        //                             join dt in db.TDoctorTreatments on d.Id equals dt.DoctorId
        //                             join l in db.MLocations on mf.LocationId equals l.Id
        //                             join ll in db.MLocationLevels on l.LocationLevelId equals ll.Id

        //                             where d.IsDelete == false
        //                             select new VMMBiodata
        //                             {
        //                                 Id = b.Id,
        //                                 Fullname = b.Fullname,
        //                                 ImagePath = b.ImagePath,
        //                                 Specialization_Name = s.Name,
        //                                 Treatment_Name = dt.Name,
        //                                 Experience = now.Year - dO.StartDate.Year,
        //                                 Hospital_Name = mf.Name,
        //                                 Location_Name = l.Name,


        //                             }).ToList();
        //    return data;
        //}
        [HttpGet("CariAllData")]
        public List<VMCariDokter> CariAllData()
        {
            DateTime now = DateTime.Now;
            List<VMCariDokter> data = (from b in db.MBiodata
                                       join d in db.MDoctors on b.Id equals d.BiodataId
                                       join cd in db.TCurrentDoctorSpecializations on d.Id equals cd.DoctorId
                                       join s in db.MSpecializations on cd.SpecializationId equals s.Id
                                       join dt in db.TDoctorTreatments on d.Id equals dt.DoctorId                                   
                                       select new VMCariDokter
                                       {
                                           Id = b.Id,
                                           DoctorId = d.Id,
                                           Fullname = b.Fullname,
                                           ImagePath = b.ImagePath,
                                           Specialization_Name = s.Name,
                                           Treatment_Name = dt.Name,
                                           ListMedicalFacility = (from office in db.TDoctorOffices
                                                                  join mf1 in db.MMedicalFacilities on office.MedicalFacilityId equals mf1.Id
                                                                  join lc in db.MLocations on mf1.LocationId equals lc.Id
                                                                  join llc in db.MLocationLevels on lc.LocationLevelId equals llc.Id
                                                                  where office.DoctorId == d.Id
                                                                  select new VMCariDokter
                                                                  {                                                                    
                                                                      Experience = now.Year - office.StartDate.Year,
                                                                      Hospital_Name = mf1.Name,
                                                                      Location_Name = lc.Name,
                                                                      Abbreviation = llc.Abbreviation,
                                                                  }).OrderByDescending(a => a.Experience).ToList()
                                       }).ToList();
            return data;
        }


        [HttpGet("GetAllDataLokasi")]
        public List<MLocation> GetAllDataLokasi()
        {
            List<MLocation> data = db.MLocations.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetAllDataSpecialization")]
        public List<MSpecialization> GetAllDataSpecialization()
        {
            List<MSpecialization> data = db.MSpecializations.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetAllDataTreatment")]
        public List<TDoctorTreatment> GetAllDataTreatment()
        {
            List<TDoctorTreatment> data = db.TDoctorTreatments.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetAllDataLocationLevel")]

        public List<MLocationLevel> GetAllDataLocationLevel()
        {
            List<MLocationLevel> data = db.MLocationLevels.Where(a => a.IsDelete== false).ToList();
            return data;
        }

      
    }
}
