﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class apiAlamatController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private AlamatService alamatServices;
        private int IdUser = 1; //

        public apiAlamatController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMLocation> GetAllData()
        {
            List<VMLocation> data = (from ba in db.MBiodataAddresses
                                     join b in db.MBiodata on ba.BiodataId equals b.Id
                                     join l in db.MLocations on ba.LocationId equals l.Id
                                     join ll in db.MLocationLevels on l.LocationLevelId equals ll.Id
                                     where ba.IsDelete == false
                                     select new VMLocation
                                     {
                                         Id = ba.Id,

                                         BiodataId = b.Id,
                                         Fullname = b.Fullname,

                                         Label = ba.Label,
                                         Recipient = ba.Recipient,
                                         RecipientPhoneNumber = ba.RecipientPhoneNumber,
                                         PostalCode = ba.PostalCode,
                                         Address = ba.Address,

                                         LocationId = ba.LocationId,
                                         NameLocation = l.Name,
                                         LocationLevelId = ll.Id,

                                         NameLevelLocation = ll.Name
                                     }).ToList();
            return data;                                                                                     
        }

        [HttpPost("Submit")]
        public VMResponse Save(VMLocation data)
        {
            MBiodataAddress dataBioAdress = new MBiodataAddress();
            dataBioAdress.BiodataId = data.BiodataId;
            dataBioAdress.Label = data.Label;
            dataBioAdress.Recipient = data.Recipient;
            dataBioAdress.RecipientPhoneNumber = data.RecipientPhoneNumber;
            dataBioAdress.LocationId = data.LocationId;
            dataBioAdress.PostalCode = data.PostalCode;
            dataBioAdress.Address = data.Address;
            dataBioAdress.CreatedBy = IdUser;
            dataBioAdress.CreatedOn = DateTime.Now;
            dataBioAdress.IsDelete = false;

            try
            {
                db.Add(dataBioAdress);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;

        }

        [HttpGet("GetBiodataId/{IdUser}")]
        public VMLocation GetBiodataId(long? IdUser)
        {
            VMLocation data = (from u in db.MUsers
                             join b in db.MBiodata on u.BiodataId equals b.Id
                             where u.Id == IdUser && u.IsDelete == false && b.IsDelete == false
                             select new VMLocation
                             {
                                 BiodataId = b.Id,
                             }).FirstOrDefault()!;
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMLocation GetDataById(int id)
        {
            VMLocation data = (from ba in db.MBiodataAddresses
                               join b in db.MBiodata on ba.BiodataId equals b.Id
                               join l in db.MLocations on ba.LocationId equals l.Id
                               join ll in db.MLocationLevels on l.LocationLevelId equals ll.Id
                               where ba.IsDelete == false && ba.Id == id
                               select new VMLocation
                               {
                                   Id = ba.Id,

                                   BiodataId = b.Id,
                                   Fullname = b.Fullname,

                                   Label = ba.Label,
                                   Recipient = ba.Recipient,
                                   RecipientPhoneNumber = ba.RecipientPhoneNumber,
                                   PostalCode = ba.PostalCode,
                                   Address = ba.Address,

                                   LocationId = ba.LocationId,
                                   NameLocation = l.Name,
                                   LocationLevelId = ll.Id,

                                   NameLevelLocation = ll.Name
                               }).FirstOrDefault()!;
            return data;
        }

        [HttpPut("MultipleDelete")]
        public VMResponse MultipleDelete(List<int> listId)
        {
            if (listId.Count > 0)
            {
                foreach (int item in listId)
                {
                    MBiodataAddress dt = db.MBiodataAddresses.Where(a => a.Id == item).FirstOrDefault()!;
                    dt.IsDelete = true;
                    dt.DeletedBy = 1;
                    dt.DeletedOn = DateTime.Now;
                    db.Update(dt);
                }

                try
                {
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }


        [HttpPost("Edit")]
        public VMResponse Edit(VMLocation data)
        {
            MBiodataAddress dt = db.MBiodataAddresses.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Label = data.Label;
                dt.Recipient = data.Recipient;
                dt.RecipientPhoneNumber = data.RecipientPhoneNumber;
                dt.LocationId = data.LocationId;
                dt.PostalCode = data.PostalCode ?? null;
                dt.Address = data.Address;
                dt.ModifiedBy = 1;
                dt.ModifiedOn = DateTime.Now;


                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success Saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }


        [HttpDelete("Delete/{id}")]

        public VMResponse Delete(long id)
        {
            MBiodataAddress dt = db.MBiodataAddresses.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.DeletedBy = 1;
                dt.DeletedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success Deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }


    }
}
