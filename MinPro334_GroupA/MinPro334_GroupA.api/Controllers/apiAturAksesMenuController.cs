﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.api.Services;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class apiAturAksesMenuController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private RolesService rolesServices;
        private int IdUser = 1; //

        public apiAturAksesMenuController(DB_SpecificationContext _db)
        {
            db = _db;
            rolesServices = new RolesService(db);
        }

        [HttpGet("GetAllData")]
        public List<MRole> GetAllData()
        {
            List<MRole> data = db.MRoles.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public async Task<VMMRole> DataById(int id)
        {
            VMMRole result = db.MRoles.Where(a => a.Id == id)
                                .Select(a => new VMMRole()
                                {
                                    Id = a.Id,
                                    Name = a.Name,
                                    CreatedBy = a.CreatedBy,
                                    Code = a.Code
                                }).FirstOrDefault()!;
            result.role_menu = await rolesServices.GetMenuAccessParentChildByRoleID(result.Id, 0, false);
            return result;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(VMMRole data)
        {
            MRole dt = db.MRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Name = data.Name;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);

                    //SAVE MenuAccess
                    List<MMenuRole> roleMenuDB = db.MMenuRoles.Where(a => a.RoleId == data.Id).ToList();

                    if (roleMenuDB.Count() > 0)
                    {
                        // delete unused data 
                        List<MMenuRole> roleMenuRemove = roleMenuDB.Where(a =>
                        !(data.role_menu.Where(b => b.is_selected && b.MenuId == a.MenuId).Select(b => b.Id)).Any()
                        ).ToList();
                        foreach (MMenuRole item in roleMenuRemove)
                        {
                            //db.Remove(item);

                            item.IsDelete = true;
                            item.ModifiedBy = IdUser;
                            item.ModifiedOn = DateTime.Now;
                            db.Update(item);
                        }

                        // update existing data
                        List<MMenuRole> roleMenuUpdate = roleMenuDB.Where(a =>
                        (data.role_menu.Where(b => b.is_selected && b.MenuId == a.MenuId).Select(b => b.Id)).Any()
                        ).ToList();
                        foreach (MMenuRole item in roleMenuUpdate)
                        {
                            if (item.IsDelete == true)
                            {
                                item.IsDelete = false;
                                item.ModifiedBy = IdUser;
                                item.ModifiedOn = DateTime.Now;
                                db.Update(item);
                            }
                        }
                    }

                    // insert new data role menu
                    List<MMenuRole> roleMenuAdd = data.role_menu.Where(a =>
                    !(roleMenuDB.Where(b => b.MenuId == a.MenuId).Select(b => b.Id)).Any() && a.is_selected
                        ).Select(a => new MMenuRole()
                        {
                            MenuId = a.MenuId,
                            RoleId = data.Id,
                            IsDelete = false,
                            CreatedBy = IdUser,
                            CreatedOn = DateTime.Now
                        }).ToList();
                    foreach (MMenuRole item in roleMenuAdd)
                    {
                        db.Add(item);
                    }

                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + e.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }

    //    [HttpPut("Edit")]
    //    public VMResponse Edit(VMMRole data)
    //    {
    //        MRole dt = db.MRoles.Where(a => a.Id == data.Id).FirstOrDefault()!;

    //        if (dt != null)
    //        {
    //            dt.Name = data.Name;
    //            dt.ModifiedBy = IdUser;
    //            dt.ModifiedOn = DateTime.Now;

    //            try
    //            {
    //                db.Update(dt);

    //                //SAVE MenuAccess
    //                if (data.role_menu.Count() > 0)
    //                {
    //                    //Remove MenuAccess
    //                    List<MMenuRole> ListMenuAccessRemove = db.MMenuRoles.Where(a => a.RoleId == data.Id).ToList();
    //                    if (ListMenuAccessRemove.Count() > 0)
    //                    {
    //                        foreach (MMenuRole item in ListMenuAccessRemove)
    //                        {
    //                            item.IsDelete = true;
    //                            item.ModifiedBy = IdUser;
    //                            item.ModifiedOn = DateTime.Now;
    //                            db.Update(item);
    //                        }
    //                    }

    //                    //Insert MenuAccess
    //                    List<MMenuRole> ListMenuAccessAdd = data.role_menu.Where(a => a.is_selected == true)
    //                                                            .Select(a => new MMenuRole()
    //                                                            {
    //                                                                RoleId = data.Id,
    //                                                                MenuId = a.MenuId,
    //                                                                IsDelete = false,
    //                                                                CreatedBy = IdUser,
    //                                                                CreatedOn = DateTime.Now

    //                                                            }).ToList();

    //                    foreach (MMenuRole item in ListMenuAccessAdd)
    //                    {
    //                        db.Add(item);
    //                    }
    //                }

    //                db.SaveChanges();

    //                respon.Message = "Data success saved";
    //            }
    //            catch (Exception e)
    //            {
    //                respon.Success = false;
    //                respon.Message = "Failed saved : " + e.Message;
    //            }
    //        }
    //        else
    //        {
    //            respon.Success = false;
    //            respon.Message = "Data not found";
    //        }

    //        return respon;
    //    }

    }
}
