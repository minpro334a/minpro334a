﻿using Microsoft.AspNetCore.Mvc;

using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiBankController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        public apiBankController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MBank> GetAllData()
        {
            List<MBank> data = db.MBanks.Where(a => a.IsDelete == false).ToList();

            return data;
        }

        [HttpGet("CheckBankByName/{name}/{id}")]
        public bool CheckBankByName(string name, long id)
        {
            MBank data = new MBank();
            if (id == 0)
            {
                data = db.MBanks.Where(a => a.Name == name && a.IsDelete == false).FirstOrDefault();
            }
            else
            {
                data = db.MBanks.Where(a => a.Name == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }


            if (data != null) // jika ada
            {
                return true;
            }

            return false;
        }

        [HttpPost("SaveBank")]
        public VMResponse Save(MBank dataParam)
        {
            MBank data = new MBank();
            data.Name = dataParam.Name;
            data.VaCode = dataParam.VaCode;
            data.CreatedBy = dataParam.CreatedBy;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Bank berhasil ditambahkan";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Penambahan gagal : " + ex.Message;
            }

            return respon;
        }

        [HttpGet("GetDataById/{id}")]
        public MBank GetDataById(long id)
        {
            MBank data = db.MBanks.Where(a => a.Id == id).FirstOrDefault();

            return data;
        }

        [HttpPut("Edit")]// tidak pake parameter {}, pake request body karna melempar class
        public VMResponse Edit(MBank dataParam)
        {
            MBank dt = db.MBanks.Where(a => a.Id == dataParam.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Name = dataParam.Name;
                dt.VaCode = dataParam.VaCode;
                dt.ModifiedBy = dataParam.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;
                dt.IsDelete = false;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data Save Success";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Data Save Failed" + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{Id}/{DeletedBy}")]
        public VMResponse Delete(int Id, int DeletedBy )
        {
            MBank dt = db.MBanks.Where(a => a.Id == Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.DeletedBy= DeletedBy;
                dt.DeletedOn = DateTime.Now;
                dt.IsDelete = true;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data Delete Success";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Data Delete Failed" + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

        [HttpGet("CheckByName/{name}")]
        public bool CheckByName(string name)
        {
            MBank data = new MBank();

            data = db.MBanks.Where(a => a.Name == name && a.IsDelete == false).FirstOrDefault();

            if (data != null) // jika ada
            {
                return true;
            }

            return false;
        }

        [HttpGet("CheckByCode/{vacode}")]
        public bool CheckByCode(string vacode)
        {
            MBank data = new MBank();
            data = db.MBanks.Where(a => a.VaCode == vacode && a.IsDelete == false).FirstOrDefault();


            if (data != null) // jika ada
            {
                return true;
            }

            return false;
        }

        [HttpGet("CheckNameCode/{name}/{vacode}")]
        public bool CheckNameCode(string name, string vacode)
        {
            MBank data = new MBank();

            data = db.MBanks.Where(a => (a.Name == name || a.VaCode == vacode) && a.IsDelete == false).FirstOrDefault();



            if (data != null) // jika ada
            {
                return true;
            }

            return false;
        }


    }
}
