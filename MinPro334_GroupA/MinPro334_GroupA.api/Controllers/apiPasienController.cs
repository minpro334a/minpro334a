﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class apiPasienController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private PasienService pasienServices;
        private int IdUser = 1; //

        public apiPasienController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMCustomerMember> GetAllData()
        {
            List<VMCustomerMember> data = (from cm in db.MCustomerMembers
                                           join b in db.MBiodata on cm.ParentBiodataId equals b.Id
                                           join c in db.MCustomers on cm.CustomerId equals c.Id
                                           join bld in db.MBloodGroups on c.BloodGroupId equals bld.Id
                                           join cr in db.MCustomerRelations on cm.CustomerRelationId equals cr.Id
                                           where cm.IsDelete == false
                                           select new VMCustomerMember
                                           {
                                               Id = cm.Id,

                                               ParentBiodataId = cm.ParentBiodataId,
                                               Fullname = b.Fullname,

                                               CustomerRelationId = cm.CustomerRelationId,
                                               Name = cr.Name,

                                               CustomerId = cm.CustomerId,
                                               Dob = c.Dob,
                                               Gender = c.Gender,
                                               RhesusType = c.RhesusType,
                                               Height = c.Height,
                                               Weight = c.Weight,

                                               BloodGroupId = c.BloodGroupId,
                                               Code = bld.Code,

                                               Age = DateTime.Now.Year - Convert.ToDateTime(c.Dob).Year                                               
                                           }).ToList();

            return data;
        }

        [HttpPost("Submit")]
        public VMResponse Save(VMCustomerMember data)
        {
            MBiodatum dataBiodata = new MBiodatum();
            dataBiodata.Fullname = data.Fullname;
            dataBiodata.CreatedBy = IdUser;
            dataBiodata.CreatedOn = DateTime.Now;
            dataBiodata.IsDelete = false;

            try
            {
                db.Add(dataBiodata);
                db.SaveChanges();

                //MBloodGroup dataBlood = db.MBloodGroups.Where(a => a.Id == data.BloodGroupId && a.IsDelete == false).FirstOrDefault();


                MCustomer dataCustomer = new MCustomer();
                dataCustomer.BiodataId = dataBiodata.Id;
                dataCustomer.Dob = data.Dob;
                dataCustomer.Gender = data.Gender;
                dataCustomer.BloodGroupId = data.BloodGroupId;
                dataCustomer.RhesusType = data.RhesusType;
                dataCustomer.Height = data.Height;
                dataCustomer.Weight = data.Weight;
                dataCustomer.CreatedBy = IdUser;
                dataCustomer.CreatedOn = DateTime.Now;
                dataCustomer.IsDelete = false;

                db.Add(dataCustomer);
                db.SaveChanges();

                //MCustomerRelation dataCustomerRelation = new MCustomerRelation();
                //dataCustomerRelation.Name = data.Name;

                MCustomerMember dataCustomerMember = new MCustomerMember();
                dataCustomerMember.ParentBiodataId = dataBiodata.Id;
                dataCustomerMember.CustomerId = dataCustomer.Id;
                dataCustomerMember.CustomerRelationId = data.CustomerRelationId;
                dataCustomerMember.CreatedBy = IdUser;
                dataCustomerMember.CreatedOn = DateTime.Now;
                dataCustomerMember.IsDelete = false;

                db.Add(dataCustomerMember);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;

        }

        [HttpGet("GetDataById/{id}")]
        public VMCustomerMember GetDataById(int id)
        {
            VMCustomerMember data = (from cm in db.MCustomerMembers
                                     join b in db.MBiodata on cm.ParentBiodataId equals b.Id
                                     join c in db.MCustomers on cm.CustomerId equals c.Id
                                     join bld in db.MBloodGroups on c.BloodGroupId equals bld.Id
                                     join cr in db.MCustomerRelations on cm.CustomerRelationId equals cr.Id
                                     where cm.IsDelete == false && cm.Id == id
                                     select new VMCustomerMember
                                     {
                                         Id = cm.Id,

                                         ParentBiodataId = cm.ParentBiodataId,
                                         Fullname = b.Fullname,

                                         CustomerRelationId = cm.CustomerRelationId,
                                         Name = cr.Name,

                                         CustomerId = cm.CustomerId,
                                         Dob = c.Dob,
                                         Gender = c.Gender,
                                         RhesusType = c.RhesusType,
                                         Height = c.Height,
                                         Weight = c.Weight,

                                         BloodGroupId = c.BloodGroupId,
                                         Code = bld.Code,

                                         Age = DateTime.Now.Year - Convert.ToDateTime(c.Dob).Year
                                     }).FirstOrDefault()!;

            return data;
        }

        [HttpPost("Edit")]
        public VMResponse Edit(VMCustomerMember data)
        {
            MCustomerMember dataCustomerMember = db.MCustomerMembers.Where(a => a.Id == data.Id).FirstOrDefault();

            if (dataCustomerMember != null)
            {
                MBiodatum dataBiodata = db.MBiodata.Where(a => a.Id == dataCustomerMember.ParentBiodataId).FirstOrDefault();
                if (dataBiodata != null)
                {
                    dataBiodata.Fullname = data.Fullname;
                    dataBiodata.ModifiedBy = IdUser;
                    dataBiodata.ModifiedOn = DateTime.Now;

                    
                    try
                    {
                        db.Update(dataBiodata);
                        db.SaveChanges();

                        respon.Message = "";
                    }
                    catch (Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "failed saved : " + ex.Message;
                    }
                }

                MCustomer dataCustomer = db.MCustomers.Where(a => a.Id == dataCustomerMember.CustomerId).FirstOrDefault();
                if (dataCustomer != null)
                {
                    dataCustomer.BiodataId = dataBiodata.Id;
                    dataCustomer.Dob = data.Dob;
                    dataCustomer.Gender = data.Gender;
                    dataCustomer.BloodGroupId = data.BloodGroupId;
                    dataCustomer.RhesusType = data.RhesusType;
                    dataCustomer.Height = data.Height;
                    dataCustomer.Weight = data.Weight;
                    dataCustomer.ModifiedBy = IdUser;
                    dataCustomer.ModifiedOn = DateTime.Now;

                    try
                    {
                        db.Update(dataCustomer);
                        db.SaveChanges();

                        respon.Message = "";
                    }
                    catch (Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "failed saved : " + ex.Message;
                    }
                }

                //dataCustomerMember.ParentBiodataId = dataBiodata.Id;
                //dataCustomerMember.CustomerId = dataCustomer.Id;
                dataCustomerMember.CustomerRelationId = data.CustomerRelationId;
                dataCustomerMember.ModifiedBy = IdUser;
                dataCustomerMember.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dataCustomerMember);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "failed saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;

        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            MCustomerMember dataCustomerMember = db.MCustomerMembers.Where(a => a.Id == id).FirstOrDefault();

            if (dataCustomerMember != null)
            {
                MBiodatum dataBiodata = db.MBiodata.Where(a => a.Id == dataCustomerMember.ParentBiodataId).FirstOrDefault();
                if (dataBiodata != null)
                {
                    dataBiodata.ModifiedBy = IdUser;
                    dataBiodata.ModifiedOn = DateTime.Now;
                    dataBiodata.IsDelete = true;

                    try
                    {
                        db.Update(dataBiodata);
                        db.SaveChanges();

                        respon.Message = "";
                    }
                    catch (Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "failed deleted : " + ex.Message;
                    }
                }

                MCustomer dataCustomer = db.MCustomers.Where(a => a.Id == dataCustomerMember.CustomerId).FirstOrDefault();
                if (dataCustomer != null)
                {
                    dataCustomer.ModifiedBy = IdUser;
                    dataCustomer.ModifiedOn = DateTime.Now;
                    dataCustomer.IsDelete = true;

                    try
                    {
                        db.Update(dataCustomer);
                        db.SaveChanges();

                        respon.Message = "";
                    }
                    catch (Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "failed deleted : " + ex.Message;
                    }
                }

                dataCustomerMember.ModifiedBy = IdUser;
                dataCustomerMember.ModifiedOn = DateTime.Now;
                dataCustomerMember.IsDelete = true;

                try
                {
                    db.Update(dataCustomerMember);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "failed deleted : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;

        }

        [HttpPut("MultipleDelete")]
        public VMResponse MultipleDelete(List<int> listId)
        {
            if (listId.Count > 0)
            {
                foreach (int item in listId)
                {
                    MCustomerMember dt = db.MCustomerMembers.Where(a => a.Id == item).FirstOrDefault();
               
                    dt.ModifiedBy = IdUser;
                    dt.ModifiedOn = DateTime.Now;
                    dt.IsDelete = true;

                    db.Update(dt);
                }

                try
                {
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                } 
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "data not found";
            }

            return respon;
        }
    }
}
