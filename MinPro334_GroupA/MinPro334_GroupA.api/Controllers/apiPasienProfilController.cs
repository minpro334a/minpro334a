﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiPasienProfilController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 2;

        public apiPasienProfilController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData/{id}")]
        public VMPasienProfil GetAllData(int id)
        {
            VMPasienProfil data = (from a in db.MUsers
                                         join b in db.MBiodata on a.BiodataId equals b.Id
                                         join c in db.MCustomers on a.Id equals c.Id
                                         where a.IsDelete == false && a.Id == id
                                         select new VMPasienProfil
                                         {
                                             Id = a.Id,
                                             BiodataId = a.BiodataId,
                                             Dob = c.Dob,

                                             Email = a.Email,
                                             Password = a.Password,

                                             Fullname = b.Fullname,
                                             MobilePhone = b.MobilePhone,
                                             ImagePath = b.ImagePath,

                                             CreatedBy = a.CreatedBy,
                                             CreatedOn = a.CreatedOn,
                                             ModifiedBy = a.ModifiedBy,
                                             ModifiedOn = a.ModifiedOn,
                                         }).FirstOrDefault()!;
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public VMPasienProfil GetDataById(int id)
        {
            VMPasienProfil data = (from a in db.MUsers
                                         join b in db.MBiodata on a.BiodataId equals b.Id
                                         where a.IsDelete == false && a.Id == id
                                         select new VMPasienProfil
                                         {
                                             Id = a.Id,
                                             BiodataId = a.BiodataId,

                                             Email = a.Email,
                                             Password = a.Password,

                                             Fullname = b.Fullname,
                                             MobilePhone = b.MobilePhone,
                                             ImagePath = b.ImagePath,

                                             CreatedBy = a.CreatedBy,
                                             CreatedOn = a.CreatedOn,
                                             ModifiedBy = a.ModifiedBy,
                                             ModifiedOn = a.ModifiedOn,
                                         }).FirstOrDefault()!;
            return data;
        }

        [HttpGet("CheckEmail/{email}/{id}")]
        public bool CheckEmail(string email, int id)
        {
            MUser data = new MUser();
            if (id == 0)
            {
                data = db.MUsers.Where(a => a.Email == email && a.IsDelete == false).FirstOrDefault()!;
            }
            else
            {
                data = db.MUsers.Where(a => a.Email == email && a.IsDelete == false && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpGet("CheckPassword/{password}/{id}")]
        public bool CheckPassword(string password, int id)
        {
            MUser data = new MUser();
            if (id == IdUser)
            {
                data = db.MUsers.Where(a => a.Password == password && a.IsDelete == false && a.Id == id).FirstOrDefault()!;
            }

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpGet("GetDataByIdBiodata/{id}")]
        public List<VMPasienProfil> GetDataByIdCategory(int id)
        {
            List<VMPasienProfil> data = (from a in db.MUsers
                                         join b in db.MBiodata on a.BiodataId equals b.Id
                                         where a.IsDelete == false && a.BiodataId == id
                                         select new VMPasienProfil
                                         {
                                             Id = a.Id,
                                             BiodataId = a.BiodataId,

                                             Email = a.Email,
                                             Password = a.Password,

                                             Fullname = b.Fullname,
                                             MobilePhone = b.MobilePhone,
                                             ImagePath = b.ImagePath,

                                             CreatedBy = a.CreatedBy,
                                             CreatedOn = a.CreatedOn,
                                             ModifiedBy = a.ModifiedBy,
                                             ModifiedOn = a.ModifiedOn,
                                         }).ToList();
            return data;
        }

        [HttpPost("Save")]
        public VMResponse Save(TToken dataParam)
        {
            List<TToken> dataCount = db.TTokens.Where(a => a.Email == dataParam.Email && a.IsDelete == false
            && a.IsExpired == false).ToList();
            if (dataCount != null)
            {
                foreach (var item in dataCount)
                {
                    item.ModifiedOn = DateTime.Now;
                    item.ModifiedBy = dataParam.ModifiedBy;
                    item.IsExpired = true;


                    try
                    {
                        db.Update(item);
                        db.SaveChanges();

                        respon.Message = "Kode Terkirim";

                    }

                    catch (Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "Failed saved" + ex.Message;


                    }
                }

            }
            TToken data = new TToken();
            data.Email = dataParam.Email;
            data.UserId = 1;
            data.Token = dataParam.Token;
            data.ExpiredOn = DateTime.Now.AddMinutes(10);
            data.IsExpired = false;

            data.UsedFor = dataParam.UsedFor;
            data.IsDelete = false;
            data.CreatedBy = 1;
            data.CreatedOn = DateTime.Now;


            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }

        [HttpPost("ReturnSend")]
        public VMResponse ReturnSend(TToken dataParam)
        {
            respon.Message = "Data success saved";
            List<TToken> dataCount = db.TTokens.Where(a => a.Email == dataParam.Email && a.IsDelete == false
            && a.IsExpired == false).ToList();
            if (dataCount != null)
            {
                foreach (var item in dataCount)
                {
                    item.ModifiedOn = DateTime.Now;
                    item.ModifiedBy = dataParam.ModifiedBy;
                    item.IsExpired = true;


                    try
                    {
                        db.Update(item);
                        db.SaveChanges();

                        respon.Message = "Verifikasi Berhasil";

                    }

                    catch (Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "Failed saved" + ex.Message;


                    }
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            TToken data = new TToken();
            data.Email = dataParam.Email;
            data.UserId = 1;
            data.Token = dataParam.Token;
            data.ExpiredOn = DateTime.Now.AddMinutes(10);
            data.IsExpired = false;

            data.UsedFor = dataParam.UsedFor;
            data.IsDelete = false;
            data.CreatedBy = 1;
            data.CreatedOn = DateTime.Now;
            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }

        [HttpGet("CheckEmailToken/{email}/{otp}")]
        public bool CheckEmailToken(string email, string otp)

        {
            TToken data = new TToken();

            data = db.TTokens.Where(a => a.Email == email && a.IsDelete == false && a.Token == otp).FirstOrDefault()!;
            if (data != null)
            {
                //DateTime now = DateTime.Now;
                //int nowint = Convert.ToInt32(now.ToString("yyyyMMddHHmmss"));
                //int expint = Convert.ToInt32(data.ExpiredOn);
                //if (DateTime.Now < data.ExpiredOn)
                //{
                //    return true;
                //}
                return true;
            }
            return false;
        }

        [HttpGet("CheckTokenExp/{email}/{otp}")]
        public bool CheckTokenExp(string email, string otp)

        {
            TToken data = new TToken();

            data = db.TTokens.Where(a => a.Email == email && a.IsDelete == false && a.Token == otp && a.IsExpired == false).FirstOrDefault()!;
            if (data != null)
            {
                //DateTime now = DateTime.Now;
                //int nowint = Convert.ToInt32(now.ToString("yyyyMMddHHmmss"));
                //int expint = Convert.ToInt32(data.ExpiredOn);
                if (DateTime.Now < data.ExpiredOn)
                {
                    return true;
                }
            }
            return false;
        }

        [HttpPost("UpdateOtp")]
        public VMResponse UpdateOtp(TToken dataParam)
        {

            List<TToken> dataCount = db.TTokens.Where(a => a.Email == dataParam.Email && a.IsDelete == false
            && a.IsExpired == false).ToList();
            if (dataCount != null)
            {
                foreach (var item in dataCount)
                {
                    item.ModifiedOn = DateTime.Now;
                    item.ModifiedBy = dataParam.ModifiedBy;
                    item.IsExpired = true;


                    try
                    {
                        db.Update(item);
                        db.SaveChanges();

                        respon.Message = "Verifikasi Berhasil";

                    }

                    catch (Exception ex)
                    {
                        respon.Success = false;
                        respon.Message = "Failed saved" + ex.Message;


                    }
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

        [HttpPut("EditUserEmail")]
        public VMResponse EditUserEmail(MUser data)
        {
            MUser dt = db.MUsers.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Email = data.Email;
                dt.ModifiedBy = data.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed Saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

        [HttpPut("EditUser")]
        public VMResponse EditUser(MUser data)
        {
            MUser dt = db.MUsers.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Password = data.Password;
                dt.ModifiedBy = data.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed Saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }


        [HttpPut("EditCustomer")]
        public VMResponse EditCustomer(MCustomer data)
        {
            MCustomer dt = db.MCustomers.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Dob = data.Dob;
                dt.ModifiedBy = data.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed Saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

        [HttpPut("EditBiodata")]
        public VMResponse EditBiodata(MBiodatum data)
        {
            MBiodatum dt = db.MBiodata.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Fullname = data.Fullname;
                dt.MobilePhone = data.MobilePhone;
                dt.ModifiedBy = data.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed Saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }



    }
}
