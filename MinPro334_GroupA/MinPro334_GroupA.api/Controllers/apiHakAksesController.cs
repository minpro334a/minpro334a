﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class apiHakAksesController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private HakAksesService hakAksesService;
        private int IdUser = 1; //

        public apiHakAksesController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MRole> GetAllData()
        {
            List<MRole> data = db.MRoles.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{id}")]
        public MRole DataById(int id)
        {
            MRole result = db.MRoles.Where(a => a.Id == id).FirstOrDefault();
            return result;
        }

        [HttpGet("CheckName/{name}/{id}")]
        public bool CheckName(string name, int id)
        {
            MRole data = new MRole();

            if (id == 0) //untuk create
            {
                data = db.MRoles.Where(a => a.Name == name && a.IsDelete == false).FirstOrDefault();
            }
            else //untuk edit
            {
                data = db.MRoles.Where(a => a.Name == name && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpGet("CheckCode/{code}/{id}")]
        public bool CheckCode(string code, int id)
        {
            MRole data = new MRole();

            if (id == 0) //untuk create
            {
                data = db.MRoles.Where(a => a.Code == code && a.IsDelete == false).FirstOrDefault();
            }
            else //untuk edit
            {
                data = db.MRoles.Where(a => a.Code == code && a.IsDelete == false && a.Id != id).FirstOrDefault();
            }

            if (data != null)
            {
                return true;
            }

            return false;
        }

        [HttpPost("Save")]
        public VMResponse Save(MRole data)
        {
           // data.CreatedBy = data.CreatedBy;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saves";

            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Faild saved" + ex.Message;

            }
            return respon;
        }

        [HttpPut("Edit")]

        public VMResponse Edit(MRole data)
        {
            MRole dt = db.MRoles.Where(a => a.Id == data.Id).FirstOrDefault();

            if (dt != null)
            {
                dt.Name = data.Name;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";

                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;


                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}/{deletedBy}")]

        public VMResponse Delete(int id, int deletedBy)
        {
            MRole dt = db.MRoles.Where(a => a.Id == id).FirstOrDefault();
            if (dt != null)
            {
                dt.IsDelete = true;
                dt.DeletedBy = deletedBy;
                dt.DeletedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";

                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;


                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }

            return respon;

        }
    }

    


}
