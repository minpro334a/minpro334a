﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiTambahSpesialisasiController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiTambahSpesialisasiController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData/{id}")]
        public List<VMTambahSpesialisasi> GetAllData(int id)
        {
            List<VMTambahSpesialisasi> data = (from a in db.TCurrentDoctorSpecializations
                                         join b in db.MSpecializations on a.SpecializationId equals b.Id
                                         join c in db.MDoctors on a.DoctorId equals c.Id
                                         where a.IsDelete == false && a.Id == id //&& b.IsDelete == false && c.IsDelete == false
                                         select new VMTambahSpesialisasi
                                         {
                                             Id = a.Id,
                                             SpecializationId = a.Id,
                                             DoctorId = a.DoctorId,
                                             NameSpesialis = b.Name,

                                             CreatedBy = a.CreatedBy,
                                             CreatedOn = a.CreatedOn,

                                             ModifiedBy = a.ModifiedBy,
                                             ModifiedOn = a.ModifiedOn,

                                             DeletedBy = a.DeletedBy,
                                             DeletedOn = a.DeletedOn,


                                         }).ToList();

            return data;
        }
        [HttpGet("GetDataById/{id}")]
        public VMTambahSpesialisasi GetDataById(int id)
        {
            VMTambahSpesialisasi data = (from a in db.TCurrentDoctorSpecializations
                                   join b in db.MSpecializations on a.SpecializationId equals b.Id
                                   where a.IsDelete == false && a.Id == id //&& b.IsDelete == false && c.IsDelete == false
                                   select new VMTambahSpesialisasi
                                   {
                                       Id = a.Id,
                                       DoctorId = a.DoctorId,
                                       SpecializationId = b.Id,
                                       NameSpesialis = b.Name,

                                       CreatedBy = a.CreatedBy,
                                       CreatedOn = a.CreatedOn,

                                       ModifiedBy = a.ModifiedBy,
                                       ModifiedOn = a.ModifiedOn,

                                       DeletedBy = a.DeletedBy,
                                       DeletedOn = a.DeletedOn,


                                   }).FirstOrDefault()!;

            return data;
        }

        //[HttpPost("Save")]
        //public VMResponse Save(TCurrentDoctorSpecialization data)
        //{
        //    data.CreatedBy = IdUser;
        //    data.CreatedOn = DateTime.Now;
        //    data.IsDelete = false;

        //    try
        //    {
        //        db.Add(data);
        //        db.SaveChanges();

        //        respon.Message = "Data success saved";
        //    }
        //    catch (Exception ex)
        //    {
        //        respon.Success = false;
        //        respon.Message = "Failed saved: " + ex.Message;

        //        // Cek inner exception
        //        if (ex.InnerException != null)
        //        {
        //            respon.Message += " Inner Exception: " + ex.InnerException.Message;
        //        }
        //    }

        //    return respon;
        //}

        [HttpPut("Edit")]
        public VMResponse Edit(TCurrentDoctorSpecialization data)
        {
            TCurrentDoctorSpecialization dt = db.TCurrentDoctorSpecializations.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.SpecializationId = data.SpecializationId;
                dt.ModifiedBy = data.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed Saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

        //[HttpDelete("Delete/{id}")]
        //public VMResponse Delete(int id)
        //{
        //    TCurrentDoctorSpecialization dt = db.TCurrentDoctorSpecializations.Where(a => a.Id == id).FirstOrDefault()!;

        //    if (dt != null)
        //    {
        //        dt.IsDelete = true;
        //        dt.DeletedOn = DateTime.Now;
        //        dt.DeletedBy = IdUser;

        //        try
        //        {
        //            db.Update(dt);
        //            db.SaveChanges();

        //            respon.Message = "Data success deleted";
        //        }
        //        catch (Exception ex)
        //        {
        //            respon.Success = false;
        //            respon.Message = "Failed deleted : " + ex.Message;
        //        }
        //    }
        //    else
        //    {
        //        respon.Success = false;
        //        respon.Message = "Data not Found";
        //    }

        //    return respon;
        //}
    }
}
