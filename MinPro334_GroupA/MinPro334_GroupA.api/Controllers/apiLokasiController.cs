﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.api.Services;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using System.Runtime.CompilerServices;
using System.Xml.Linq;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[Controller]")]
    [ApiController]
    public class apiLokasiController : Controller
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;
        apiLokasiServices  _apiLokasiServices;

        public apiLokasiController(DB_SpecificationContext _db)
        {
            db = _db;
            _apiLokasiServices = new apiLokasiServices(db);
        }

        [HttpGet("GetAllData")]
        public async Task<List<VMTblLokasi>> GetAllData()
        {
            List<VMTblLokasi> data = (from L in db.MLocations
                                      join LM in db.MLocationLevels on L.LocationLevelId equals LM.Id
                                      where L.IsDelete == false && LM.IsDelete == false
                                      select new VMTblLokasi
                                      {

                                          Id = L.Id,
                                          Name = L.Name,
                                          ParentChildName = L.Name,
                                          ParentId = L.ParentId,
                                          LocationLevelId = L.LocationLevelId,
                                          NameLocation = LM.Name,

                                      }).ToList();
            foreach (VMTblLokasi item in data)
            {
                if (item.ParentId != null)
                {
                    item.ParentData = await _apiLokasiServices.GetParentData(item.ParentId);
                    item.ParentChildName = item.ParentChildName + ", " + item.ParentData.ParentChildName;
                }
            }

            return data;
        }


        [HttpGet("GetDataById/{id}")]
        public async Task<VMTblLokasi> GetDataById(long id)
        {
            VMTblLokasi data = (from a in db.MLocations
                                join b in db.MLocationLevels on a.LocationLevelId equals b.Id
                                where a.IsDelete == false && b.IsDelete == false && a.Id == id
                                select new VMTblLokasi
                                {
                                    Id = a.Id,
                                    Name = a.Name,
                                    ParentId = a.ParentId,
                                    LocationLevelId = a.LocationLevelId,
                                    NameLocation = b.Name,
                                }).FirstOrDefault()!;


            if (data.ParentId != null)
            {

                data.ParentData = await _apiLokasiServices.GetParentData(data.ParentId);
                if (data.ParentData != null)
                {
                    data.ParentChildName = data.ParentData.Name;
                    if (data.ParentData.ParentData != null)
                    {
                        data.ParentChildName += ", " + data.ParentData.ParentData.Name;

                    }
                }
                else
                {
                    data.ParentChildName = "-";
                }


            }


            return data;
        }


        [HttpGet("GetDataByIdLevel/{id}")]
        public async Task<List<VMTblLokasi>> GetDataByIdLevel(long id)
        {
            List<VMTblLokasi> data = (from a in db.MLocations
                                       join b in db.MLocationLevels on a.LocationLevelId equals b.Id
                                       where a.IsDelete == false && a.LocationLevelId == id - 1
                                      select new VMTblLokasi
                                       {
                                           Id = a.Id,
                                           Name = a.Name,
                                           ParentChildName = a.Name,
                                           ParentId = a.ParentId,

                                           LocationLevelId = a.LocationLevelId,

                                           NameLocation = b.Name,
                                           Abbreviation = b.Abbreviation,

                                       }).ToList();

            foreach (VMTblLokasi item in data)
            {
                if (item.ParentId != null)
                {

                    item.ParentData = await _apiLokasiServices.GetParentData(item.ParentId);
                    item.ParentChildName = item.ParentChildName;
                    if (item.ParentData != null)
                    {
                        item.ParentChildName += ", " + item.ParentData.Name;
                    }


                }
            }

            return data;
        }

        [HttpGet("CheckNameIsExist/{locationLevelId}/{name}/{id}/{parentId}")]
        public bool CheckNameIsExist(long locationLevelId, string name, long id)
        {
            MLocation data = new MLocation();

            if (id == 0)//create
            {

                data = db.MLocations.Where(a => a.Name == name && a.IsDelete == false && a.LocationLevelId == locationLevelId).FirstOrDefault()!;
            }
            else//edit
            {
                data = db.MLocations.Where(a => a.Name == name && a.IsDelete == false && a.LocationLevelId == locationLevelId && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)//untuk saat edit di front end
            {
                return true;
            }
            return false;
        }
        


        [HttpPost("Save")]

        public VMResponse Save(MLocation data)
        {

            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved" + ex.Message;
            }
            return respon;
        }

        [HttpGet("GetAllDataLocationEdit/{level}")]
        public async Task<List<VMTblLokasi>> GetAllDataLocationEdit(long? level)
        {
            List<VMTblLokasi> data = (from a in db.MLocations
                                      join b in db.MLocationLevels on a.LocationLevelId equals b.Id
                                      where a.IsDelete == false && b.IsDelete == false && a.LocationLevelId == level
                                      select new VMTblLokasi
                                      {

                                          Id = a.Id,
                                          Name = a.Name,
                                          ParentId = a.ParentId,

                                          LocationLevelId = a.LocationLevelId,
                                          NameLocation = b.Name,
                                      }).ToList();
            foreach (VMTblLokasi item in data)
            {
                if (item.ParentId != null)
                {

                    item.ParentData = await _apiLokasiServices.GetParentData(item.ParentId);
                    item.ParentName = item.ParentData.Name;
                    if (item.ParentData != null)
                    {
                        item.ParentChildName = item.ParentData.Name;
                        if (item.ParentData.ParentData != null)
                        {
                            item.ParentChildName += ", " + item.ParentData.ParentData.Name;

                        }
                    }
                    else
                    {
                        item.ParentChildName = "-";
                    }


                }
            }

            return data;
        }


        [HttpPut("Edit")]
        public VMResponse Edit(MLocation data)
        {
            MLocation dt = db.MLocations.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Name = data.Name;
                dt.LocationLevelId = data.LocationLevelId;
                dt.ParentId = data.ParentId;

                dt.ModifiedBy = 1;
                dt.ModifiedOn = DateTime.Now;


                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }


        [HttpGet("CheckWilayahDigunakan/{id}")]
        public bool CheckWilayahDigunakan(long id)
        {
            MLocation data = new MLocation();

            data = db.MLocations.Where(a => a.ParentId == id && a.IsDelete == false).FirstOrDefault()!;



            if (data != null)// _> edit @frontend
            {
                return true;
            }
            return false;
        }


        [HttpDelete("Delete/{id}")]

        public VMResponse Delete(long id)
        {
            MLocation dt = db.MLocations.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.ModifiedBy = 1;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();


                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted" + ex.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }
            return respon;
        }


    }
}
