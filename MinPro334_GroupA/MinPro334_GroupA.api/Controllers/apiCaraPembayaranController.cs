﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCaraPembayaranController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();

        public apiCaraPembayaranController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MPaymentMethod> GetAllData()
        {
            List<MPaymentMethod> data = db.MPaymentMethods.Where(a => a.IsDelete == false).ToList();

            return data;
        }

        [HttpGet("CheckPaymentMethodByName/{name}")]
        public bool CheckPaymentMethodByName(string name)
        {
            MPaymentMethod data = new MPaymentMethod();

            data = db.MPaymentMethods.Where(a => a.Name == name && a.IsDelete == false).FirstOrDefault();

            if (data != null) // jika ada
            {
                return true;
            }

            return false;
        }

        [HttpPost("SaveName")]
        public VMResponse Save(MPaymentMethod dataParam)
        {
            MPaymentMethod data = new MPaymentMethod();
            data.Name = dataParam.Name;
            data.CreatedBy = dataParam.CreatedBy;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Cara Pembayaran berhasil ditambahkan";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Penambahan gagal : " + ex.Message;
            }

            return respon;
        }
        [HttpGet("GetDataById/{id}")]
        public MPaymentMethod GetDataById(long id)
        {
            MPaymentMethod data = db.MPaymentMethods.Where(a => a.Id == id).FirstOrDefault();

            return data;
        }

        [HttpPut("Edit")]// tidak pake parameter {}, pake request body karna melempar class
        public VMResponse Edit(MPaymentMethod dataParam)
        {
            MPaymentMethod dt = db.MPaymentMethods.Where(a => a.Id == dataParam.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Name = dataParam.Name;
                dt.ModifiedBy = dataParam.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;
                dt.IsDelete = false;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data Save Success";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Data Save Failed" + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }
        [HttpDelete("Delete/{Id}/{DeletedBy}")]
        public VMResponse Delete(int Id, int DeletedBy)
        {
            MPaymentMethod dt = db.MPaymentMethods.Where(a => a.Id == Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.DeletedBy = DeletedBy;
                dt.DeletedOn = DateTime.Now;
                dt.IsDelete = true;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data Delete Success";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Data Delete Failed" + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }



    }
}
