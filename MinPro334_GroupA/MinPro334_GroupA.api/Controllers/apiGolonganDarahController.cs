﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.Services;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("/[controller]")]
    [ApiController]
    public class apiGolonganDarahController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private GolonganDarahService golonganDarahServices;
        private int IdUser = 1; //

        public apiGolonganDarahController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MBloodGroup> GetAllData()
        {
            List<MBloodGroup> data = db.MBloodGroups.Where(a => a.IsDelete == false).ToList();
            return data;
        }
    }
}
