﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiObatController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiObatController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllDataOrderHeader")]
        public List<VMMedicalItemPurchaseDetail> GetAllDataHeader()
        {
            List<VMMedicalItemPurchaseDetail> data = (from a in db.TMedicalItemPurchaseDetails
                                                      join b in db.MMedicalItems on a.MedicalItemId equals b.Id
                                                      join c in db.MMedicalItemCategories on b.MedicalItemCategoryId equals c.Id
                                                      join d in db.MMedicalItemSegmentations on b.MedicalItemSegmentationId equals d.Id
                                                      where a.IsDelete == false
                                                      select new VMMedicalItemPurchaseDetail 
                                                      { 
                                                          Id = a.Id,
                                                          MedicalItemId = a.MedicalItemId,

                                                          Qty = a.Qty,
                                                          SubTotal = a.SubTotal,

                                                          Name = b.Name,
                                                          NameCategory = c.Name,
                                                          NameSegmentation = d.Name,

                                                          MedicalItemCategoryId = c.Id,
                                                          MedicalItemSegmentationId = d.Id,

                                                          Indication = b.Indication,
                                                          Caution = b.Caution,
                                                          Packaging = b.Packaging,

                                                          PriceMax = b.PriceMax,
                                                          PriceMin = b.PriceMin,

                                                          ImagePath = b.ImagePath,

                                                          CreatedBy = a.CreatedBy,
                                                          CreatedOn = a.CreatedOn

                                                      }).ToList();
            return data;
        }



    }
}
