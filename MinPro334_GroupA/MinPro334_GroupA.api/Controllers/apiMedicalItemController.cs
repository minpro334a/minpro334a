﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiMedicalItemController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiMedicalItemController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<VMMMedicalItem> GetAllData()
        {
            List<VMMMedicalItem> data = (from a in db.MMedicalItems
                                         join b in db.MMedicalItemCategories on a.MedicalItemCategoryId equals b.Id
                                         join c in db.MMedicalItemSegmentations on a.MedicalItemSegmentationId equals c.Id
                                         where a.IsDelete == false //&& b.IsDelete == false && c.IsDelete == false
                                         select new VMMMedicalItem
                                         {
                                             Id = a.Id,
                                             Name = a.Name,
                                             Composition = a.Composition,
                                             Manufacturer = a.Manufacturer,
                                             Indication = a.Indication,
                                             Dosage = a.Dosage,
                                             Directions = a.Directions,
                                             Contraindication = a.Contraindication,
                                             Caution = a.Caution,
                                             Packaging = a.Packaging,
                                             PriceMax = a.PriceMax,
                                             PriceMin = a.PriceMin,
                                             ImagePath = a.ImagePath,

                                             MedicalItemCategoryId = a.MedicalItemCategoryId,
                                             NameCategory = b.Name,

                                             MedicalItemSegmentationId = a.MedicalItemSegmentationId,
                                             NameSegmentation = c.Name,

                                             CreatedBy = a.CreatedBy,
                                             CreatedOn = a.CreatedOn,

                                             ModifiedBy = a.ModifiedBy,
                                             ModifiedOn = a.ModifiedOn,

                                             DeletedBy = a.DeletedBy,
                                             DeletedOn = a.DeletedOn,


                                         }).ToList();

            return data;
        }



        [HttpGet("GetDataById/{id}")]
        public VMMMedicalItem GetDataById(int id)
        {
            VMMMedicalItem data = (from a in db.MMedicalItems
                                   join b in db.MMedicalItemCategories on a.MedicalItemCategoryId equals b.Id
                                   join c in db.MMedicalItemSegmentations on a.MedicalItemSegmentationId equals c.Id
                                   where a.IsDelete == false && a.Id == id //&& b.IsDelete == false && c.IsDelete == false && a.Id == id
                                   select new VMMMedicalItem
                                   {
                                       Id = a.Id,
                                       Name = a.Name,
                                       Composition = a.Composition,
                                       Manufacturer = a.Manufacturer,
                                       Indication = a.Indication,
                                       Dosage = a.Dosage,
                                       Directions = a.Directions,
                                       Contraindication = a.Contraindication,
                                       Caution = a.Caution,
                                       Packaging = a.Packaging,
                                       PriceMax = a.PriceMax,
                                       PriceMin = a.PriceMin,
                                       ImagePath = a.ImagePath,

                                       MedicalItemCategoryId = b.Id,
                                       NameCategory = b.Name,

                                       MedicalItemSegmentationId = c.Id,
                                       NameSegmentation = c.Name,

                                       CreatedBy = a.CreatedBy,
                                       CreatedOn = a.CreatedOn,

                                       ModifiedBy = a.ModifiedBy,
                                       ModifiedOn = a.ModifiedOn,

                                       DeletedBy = a.DeletedBy,
                                       DeletedOn = a.DeletedOn,


                                   }).FirstOrDefault()!;

            return data;
        }

        [HttpGet("CheckByName/{name}/{id}/{idSegmentation}/{idCategory}")]
        public bool CheckName(string name, int id, int medicalItemSegmentationId, int medicalItemCategoryId)
        {
            try
            {
                MMedicalItem data;

                if (id == 0)
                {
                    data = db.MMedicalItems
                        .Where(a => a.Name == name && a.IsDelete == false
                                     && a.MedicalItemSegmentationId == medicalItemSegmentationId
                                     && a.MedicalItemCategoryId == medicalItemCategoryId)
                        .FirstOrDefault()!;
                }
                else
                {
                    data = db.MMedicalItems
                        .Where(a => a.Name == name && a.IsDelete == false
                                     && a.MedicalItemSegmentationId == medicalItemSegmentationId
                                     && a.MedicalItemCategoryId == medicalItemCategoryId
                                     && a.Id != id)
                        .FirstOrDefault()!;
                }

                return data != null;
            }
            catch (Exception ex)
            {
                // Tangkap dan laporkan pengecualian
                Console.WriteLine($"Exception in CheckName: {ex.Message}");
                return false;
            }
        }

        [HttpPost("Save")]
        public VMResponse Save(MMedicalItem data)
        {
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved: " + ex.Message;

                // Cek inner exception
                if (ex.InnerException != null)
                {
                    respon.Message += " Inner Exception: " + ex.InnerException.Message;
                }
            }

            return respon;
        }


        [HttpPut("Edit")]
        public VMResponse Edit(MMedicalItem data)
        {
            MMedicalItem dt = db.MMedicalItems.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Name = data.Name;
                dt.MedicalItemCategoryId = data.MedicalItemCategoryId;
                dt.MedicalItemSegmentationId = data.MedicalItemSegmentationId;
                dt.PriceMax = data.PriceMax;
                dt.PriceMin = data.PriceMin;
                dt.Composition = data.Composition;
                dt.Manufacturer = data.Manufacturer;
                dt.Caution = data.Caution;
                dt.Directions = data.Directions;
                dt.Contraindication = data.Contraindication;
                dt.Directions = data.Directions;
                dt.Dosage = data.Dosage;
                dt.Packaging = data.Packaging;
                dt.Indication = data.Indication;
                if (data.ImagePath != null)
                {
                    dt.ImagePath = data.ImagePath;
                }
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed Saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            MMedicalItem dt = db.MMedicalItems.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.DeletedOn = DateTime.Now;
                dt.DeletedBy = IdUser;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success deleted";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed deleted : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }

            return respon;
        }
    }

}
