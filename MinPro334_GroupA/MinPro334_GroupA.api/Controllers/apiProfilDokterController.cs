﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;
using System.Linq;
using System.Runtime.Intrinsics.Arm;
using System.Xml.Linq;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("Profil")]
    [ApiController]
    public class apiDoctorController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;


        public apiDoctorController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetDataById/{id}")]
        public VMTblProfilDokter GetDataById(int id)
        {
            VMTblProfilDokter data = (from a in db.MDoctors
                                      join b in db.TDoctorOffices on a.Id equals b.DoctorId
                                      join c in db.MMedicalFacilities on b.MedicalFacilityId equals c.Id
                                      join e in db.MDoctorEducations on a.Id equals e.DoctorId
                                      join f in db.MBiodata on a.BiodataId equals f.Id
                                      join g in db.TCurrentDoctorSpecializations on a.Id equals g.DoctorId
                                      join h in db.MSpecializations on g.SpecializationId equals h.Id
                                      join i in db.MLocations on c.LocationId equals i.Id
                                      join j in db.MLocationLevels on i.LocationLevelId equals j.Id
                                      where a.IsDelete == false && a.Id == id
                                      select new VMTblProfilDokter
                                      {
                                          DoctorId = a.Id,
                                          NameDoctor = f.Fullname,
                                          Specialization = h.Name,
                                          Img = f.ImagePath,
                                          
                                          //List Riwayat Praktek
                                          ListNameHospital = (from td in db.TDoctorOffices
                                                              join mm in db.MMedicalFacilities on td.MedicalFacilityId equals mm.Id
                                                              join dot in db.TDoctorOfficeTreatments on td.Id equals dot.DoctorOfficeId
                                                              join dotp in db.TDoctorOfficeTreatmentPrices on dot.Id equals dotp.DoctorOfficeTreatmentId
                                                              where td.DoctorId == a.Id && mm.IsDelete == false
                                                              select new VMTblProfilDokter
                                                              {
                                                                  IdNameHospital = mm.Id,
                                                                  PriceStartFrom = dotp.PriceStartFrom,
                                                                  NameHospital = mm.Name,
                                                                  SpecializationFaskes = td.Specialization,
                                                                  Lokasi = mm.FullAddress,
                                                                  StartDate = td.StartDate,
                                                                  EndDate = td.EndDate,
                                                                  ListJadwalPraktekWaktu = (from ac in db.MMedicalFacilitySchedules
                                                                                            join ad in db.TDoctorOfficeSchedules on ac.Id equals ad.MedicalFacilityScheduleId
                                                                                            join af in db.MDoctors on ad.DoctorId equals af.Id
                                                                                            join ag in db.MMedicalFacilities on ac.MedicalFacilityId equals ag.Id
                                                                                            where af.Id == a.Id && ac.MedicalFacilityId == mm.Id && ad.IsDelete == false && ac.IsDelete == false
                                                                                            select new VMTblProfilDokter
                                                                                            {
                                                                                                Day = ac.Day,
                                                                                                JamMulai = ac.TimeScheduleStart,
                                                                                                JamSelesai = ac.TimeScheduleEnd
                                                                                            }).ToList()
                                                              }).OrderBy(a => a.NameHospital).ToList(),

                                          //ListJumlahJanji
                                          ListJumlahJanji = (from m in db.TAppointments
                                                             join n in db.MDoctors on m.DoctorOfficeId equals n.Id
                                                             where m.DoctorOfficeId == a.Id && m.IsDelete == false
                                                             select new VMTblProfilDokter
                                                             {
                                                                 IdAppointment = m.Id
                                                             }).OrderBy(a => a.IdAppointment).ToList(),
                                          ListJanjiDone = (from tap in db.TAppointments
                                                           join doff in db.TDoctorOffices on tap.DoctorOfficeId equals doff.Id
                                                           join tappd in db.TAppointmentDones on tap.Id equals tappd.AppointmentId
                                                           //into tdo from tdone in tdo.DefaultIfEmpty()
                                                           where doff.DoctorId == a.Id && tap.IsDelete == false
                                                           select new VMTblProfilDokter
                                                           {
                                                               IdAppointmentDone = tappd.Id
                                                           }
                                                           ).ToList(),
                                          ListJanjiCancel = (from tap in db.TAppointments
                                                             join doff in db.TDoctorOffices on tap.DoctorOfficeId equals doff.Id
                                                             join tapc in db.TAppointmentCancellations on tap.Id equals tapc.AppointmentId
                                                             //into tc from tcancel in tc.DefaultIfEmpty()
                                                             where doff.DoctorId == a.Id && tap.IsDelete == false
                                                             select new VMTblProfilDokter
                                                             {
                                                                 IdAppointmentCancel = tapc.Id
                                                             }).ToList(),

                                          //List Konsultasi jumlah
                                          ListKonsultasi = (from cc in db.MDoctors
                                                            join mdoc in db.TCustomerChats on cc.Id equals mdoc.DoctorId//cek lagi
                                                            //into tk from tkonsul in tk.DefaultIfEmpty()
                                                            where cc.Id == a.Id && cc.IsDelete == false
                                                            select new VMTblProfilDokter
                                                            {
                                                                //IdKonsul = tkonsul.Id == null ? 0 : tkonsul.Id
                                                                IdKonsul = mdoc.Id
                                                            }).ToList(),


                                          StartDate = b.StartDate,
                                          EndDate = b.EndDate,
                                          ListInstitutionName = (from o in db.MDoctors
                                                                 join p in db.MDoctorEducations on o.Id equals p.DoctorId
                                                                 where p.DoctorId == a.Id && p.IsDelete == false
                                                                 select new VMTblProfilDokter
                                                                 {
                                                                     InstitutionName = p.InstitutionName,
                                                                     Lulus = p.EndYear,
                                                                     Major = p.Major
                                                                 }).OrderBy(a => a.InstitutionName).ToList(),
                                          Major = e.Major,
                                          Lulus = e.EndYear,
                                          NameLevelLocation = j.Name,
                                          MedicalFacilityId = c.Id,

                                          ListJadwalPraktek = (from tdofs in db.TDoctorOfficeSchedules
                                                               join mmfs in db.MMedicalFacilitySchedules on tdofs.MedicalFacilityScheduleId equals mmfs.Id
                                                               join mdic in db.MMedicalFacilities on mmfs.MedicalFacilityId equals mdic.Id
                                                               join tdo in db.TDoctorOffices on mdic.Id equals tdo.MedicalFacilityId
                                                               join dot in db.TDoctorOfficeTreatments on tdo.Id equals dot.DoctorOfficeId
                                                               join dotp in db.TDoctorOfficeTreatmentPrices on dot.Id equals dotp.DoctorOfficeTreatmentId
                                                               where tdofs.DoctorId == a.Id && tdofs.IsDelete == false
                                                               select new VMTblProfilDokter
                                                               {
                                                                   ScheduleId = mmfs.Id,
                                                                   NameHospital = mdic.Name,
                                                                   SpecializationFaskes = tdo.Specialization,
                                                                   Lokasi = mdic.FullAddress,
                                                                   PriceStartFrom = dotp.PriceStartFrom,
                                                                   Day = mmfs.Day,
                                                                   JamMulai = mmfs.TimeScheduleStart,
                                                                   JamSelesai = mmfs.TimeScheduleEnd
                                                                   //ListJadwalPraktekWaktu = (from tdocsc in db.TDoctorOfficeSchedules
                                                                   //                          join sceid in db.MMedicalFacilitySchedules on tdocsc.MedicalFacilityScheduleId equals sceid.Id
                                                                   //                          where tdocsc.DoctorId == a.Id && sceid.IsDelete == false
                                                                   //                          select new VMTblProfilDokter
                                                                   //                          {
                                                                   //                              Day = sceid.Day,
                                                                   //                              JamMulai = sceid.TimeScheduleStart,
                                                                   //                              JamSelesai = sceid.TimeScheduleEnd
                                                                   //                          }).ToList()
                                                               }).ToList(),

                                          CreatedBy = b.CreatedBy,
                                          CreatedOn = b.CreatedOn,

                                          ListNameTreatment = (from d in db.TDoctorTreatments
                                                               where d.DoctorId == a.Id && d.IsDelete == false
                                                               select new VMTblProfilDokter
                                                               {
                                                                   NameTreatment = d.Name
                                                               }).OrderBy(a => a.NameTreatment).ToList()
                                      }).FirstOrDefault()!;

            return data;

            //MBiodatum result = db.MBiodata.Where(a => a.Id == id).FirstOrDefault()!;
            //return result
        }

        [HttpGet("GetAllDataDokter")]
        public List<VMCariDokter> GetAllDataDokter()
        {
            DateTime now = DateTime.Now;
            Console.Write(now.Date.ToString("yyyy-MM-dd"));

            List<VMCariDokter> data = (from b in db.MBiodata
                                       join d in db.MDoctors on b.Id equals d.BiodataId
                                       where d.IsDelete == false
                                       select new VMCariDokter
                                       {
                                           DoctorId = d.Id,
                                           Fullname = b.Fullname,
                                           Img = b.ImagePath,
                                           ListNameRs = (from q in db.TDoctorOffices
                                                         join c in db.MMedicalFacilities on q.MedicalFacilityId equals c.Id
                                                         where q.DoctorId == d.Id && q.IsDelete == false
                                                         select new VMCariDokter
                                                         {
                                                             MedicalFacilityName = c.Name
                                                         }).ToList()

                                           //                                           Experience = now.Year - office.StartDate.Year
                                       }).ToList();
            return data;
        }

        [HttpPost("SaveImg")]
        public VMResponse Save(MBiodatum data)
        {
            MDoctor doctor = db.MDoctors.Where(a => a.Id == data.Id).FirstOrDefault()!;
            MBiodatum dt = db.MBiodata.Where(a => a.Id == doctor.BiodataId && a.IsDelete == false).FirstOrDefault()!;

            dt.ImagePath = data.ImagePath;
            dt.IsDelete = false;
            dt.ModifiedBy = 1;
            dt.ModifiedOn = DateTime.Now;
            try
            {
                db.Update(dt);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed Saved : " + ex.Message;
            }

            return respon;
        }





        //----------------------------------------------Treatment------------------------------------------\\
        [HttpGet("GetDataByIdforTreatment")]
        public VMTblProfilDokter GetDataByIdforTreatment()
        {
            VMTblProfilDokter data = (from a in db.MDoctors
                                      join b in db.TDoctorOffices on a.Id equals b.DoctorId
                                      join c in db.MMedicalFacilities on b.MedicalFacilityId equals c.Id
                                      join e in db.MDoctorEducations on a.Id equals e.DoctorId
                                      join f in db.MBiodata on a.BiodataId equals f.Id
                                      join g in db.TCurrentDoctorSpecializations on a.Id equals g.DoctorId
                                      join h in db.MSpecializations on g.SpecializationId equals h.Id
                                      join i in db.MLocations on c.LocationId equals i.Id
                                      join j in db.MLocationLevels on i.LocationLevelId equals j.Id
                                      where a.IsDelete == false && a.Id == 1
                                      select new VMTblProfilDokter
                                      {
                                          DoctorId = a.Id,
                                          NameDoctor = f.Fullname,
                                          Specialization = h.Name,
                                          Img = f.ImagePath,

                                          ListNameHospital = (from td in db.TDoctorOffices
                                                              join mm in db.MMedicalFacilities on td.MedicalFacilityId equals mm.Id
                                                              join dot in db.TDoctorOfficeTreatments on td.Id equals dot.DoctorOfficeId
                                                              join dotp in db.TDoctorOfficeTreatmentPrices on dot.Id equals dotp.DoctorOfficeTreatmentId
                                                              where td.DoctorId == a.Id && mm.IsDelete == false
                                                              select new VMTblProfilDokter
                                                              {
                                                                  IdNameHospital = mm.Id,
                                                                  PriceStartFrom = dotp.PriceStartFrom,
                                                                  NameHospital = mm.Name,
                                                                  SpecializationFaskes = td.Specialization,
                                                                  Lokasi = mm.FullAddress,
                                                                  StartDate = td.StartDate,
                                                                  EndDate = td.EndDate,
                                                                  ListJadwalPraktekWaktu = (from ac in db.MMedicalFacilitySchedules
                                                                                            join ad in db.TDoctorOfficeSchedules on ac.Id equals ad.MedicalFacilityScheduleId
                                                                                            join af in db.MDoctors on ad.DoctorId equals af.Id
                                                                                            join ag in db.MMedicalFacilities on ac.MedicalFacilityId equals ag.Id
                                                                                            where ac.MedicalFacilityId == mm.Id && ad.IsDelete == false && ac.IsDelete == false
                                                                                            select new VMTblProfilDokter
                                                                                            {
                                                                                                Day = ac.Day,
                                                                                                JamMulai = ac.TimeScheduleStart,
                                                                                                JamSelesai = ac.TimeScheduleEnd
                                                                                            }).ToList()
                                                              }).OrderBy(a => a.NameHospital).ToList(),

                                          ListJumlahJanji = (from m in db.TAppointments
                                                             join n in db.MDoctors on m.DoctorOfficeId equals n.Id
                                                             where m.DoctorOfficeId == a.Id && m.IsDelete == false
                                                             select new VMTblProfilDokter
                                                             {
                                                                 IdAppointment = m.Id
                                                             }).OrderBy(a => a.IdAppointment).ToList(),
                                          ListJanjiDone = (from tap in db.TAppointments
                                                           join doff in db.TDoctorOffices on tap.DoctorOfficeId equals doff.Id
                                                           join tappd in db.TAppointmentDones on tap.Id equals tappd.AppointmentId
                                                           //into tdo from tdone in tdo.DefaultIfEmpty()
                                                           where doff.DoctorId == a.Id && tap.IsDelete == false
                                                           select new VMTblProfilDokter
                                                           {
                                                               IdAppointmentDone = tappd.Id
                                                           }
                                                           ).ToList(),
                                          ListJanjiCancel = (from tap in db.TAppointments
                                                             join doff in db.TDoctorOffices on tap.DoctorOfficeId equals doff.Id
                                                             join tapc in db.TAppointmentCancellations on tap.Id equals tapc.AppointmentId
                                                             //into tc from tcancel in tc.DefaultIfEmpty()
                                                             where doff.DoctorId == a.Id && tap.IsDelete == false
                                                             select new VMTblProfilDokter
                                                             {
                                                                 IdAppointmentCancel = tapc.Id
                                                             }).ToList(),
                                          ListKonsultasi = (from cc in db.MDoctors
                                                            join mdoc in db.TCustomerChats on cc.Id equals mdoc.DoctorId//cek lagi
                                                            //into tk from tkonsul in tk.DefaultIfEmpty()
                                                            where cc.Id == a.Id && cc.IsDelete == false
                                                            select new VMTblProfilDokter
                                                            {
                                                                //IdKonsul = tkonsul.Id == null ? 0 : tkonsul.Id
                                                                IdKonsul = mdoc.Id
                                                            }).ToList(),

                                          //ListSpecializationFaskes = (from anj in db.TDoctorOffices
                                          //                        join bol in db.TDoctorOffices on anj.Id equals bol.DoctorId
                                          //                        where bol.DoctorId == a.Id && bol.IsDelete == false
                                          //                        select new VMTblProfilDokter
                                          //                        {


                                          //                        }).OrderBy(a => a.Specialization).ToList(),
                                          StartDate = b.StartDate,
                                          EndDate = b.EndDate,
                                          ListInstitutionName = (from o in db.MDoctors
                                                                 join p in db.MDoctorEducations on o.Id equals p.DoctorId
                                                                 where p.DoctorId == a.Id && p.IsDelete == false
                                                                 select new VMTblProfilDokter
                                                                 {
                                                                     InstitutionName = p.InstitutionName,
                                                                     Lulus = p.EndYear,
                                                                     Major = p.Major
                                                                 }).OrderBy(a => a.InstitutionName).ToList(),
                                          Major = e.Major,
                                          Lulus = e.EndYear,
                                          NameLevelLocation = j.Name,
                                          MedicalFacilityId = c.Id,

                                          ListJadwalPraktek = (from tdofs in db.TDoctorOfficeSchedules
                                                               join mmfs in db.MMedicalFacilitySchedules on tdofs.MedicalFacilityScheduleId equals mmfs.Id
                                                               join mdic in db.MMedicalFacilities on mmfs.MedicalFacilityId equals mdic.Id
                                                               join tdo in db.TDoctorOffices on mdic.Id equals tdo.MedicalFacilityId
                                                               join dot in db.TDoctorOfficeTreatments on tdo.Id equals dot.DoctorOfficeId
                                                               join dotp in db.TDoctorOfficeTreatmentPrices on dot.Id equals dotp.DoctorOfficeTreatmentId
                                                               where tdofs.DoctorId == a.Id && tdofs.IsDelete == false
                                                               select new VMTblProfilDokter
                                                               {
                                                                   ScheduleId = mmfs.Id,
                                                                   NameHospital = mdic.Name,
                                                                   SpecializationFaskes = tdo.Specialization,
                                                                   Lokasi = mdic.FullAddress,
                                                                   PriceStartFrom = dotp.PriceStartFrom,
                                                                   Day = mmfs.Day,
                                                                   JamMulai = mmfs.TimeScheduleStart,
                                                                   JamSelesai = mmfs.TimeScheduleEnd
                                                                   //ListJadwalPraktekWaktu = (from tdocsc in db.TDoctorOfficeSchedules
                                                                   //                          join sceid in db.MMedicalFacilitySchedules on tdocsc.MedicalFacilityScheduleId equals sceid.Id
                                                                   //                          where tdocsc.DoctorId == a.Id && sceid.IsDelete == false
                                                                   //                          select new VMDoctorProfile
                                                                   //                          {
                                                                   //                              Day = sceid.Day,
                                                                   //                              JamMulai = sceid.TimeScheduleStart,
                                                                   //                              JamSelesai = sceid.TimeScheduleEnd
                                                                   //                          }).ToList()
                                                               }).ToList(),

                                          CreatedBy = b.CreatedBy,
                                          CreatedOn = b.CreatedOn,

                                          ListNameTreatment = (from q in db.MDoctors
                                                               join d in db.TDoctorTreatments on q.Id equals d.DoctorId
                                                               where d.DoctorId == a.Id && d.IsDelete == false
                                                               select new VMTblProfilDokter
                                                               {
                                                                   TreatmentId = d.Id,
                                                                   NameTreatment = d.Name
                                                               }).OrderBy(a => a.NameTreatment).ToList()
                                      }).FirstOrDefault()!;

            return data;

            //MBiodatum result = db.MBiodata.Where(a => a.Id == id).FirstOrDefault()!;
            //return result
        }

        [HttpGet("CheckTreatmentByName/{name}/{id}")]
        public bool CheckName(string name, int id)
        {
            TDoctorTreatment data = new TDoctorTreatment();
            if (id == 0)
            {
                data = db.TDoctorTreatments.Where(a => a.Name == name && a.IsDelete == false).FirstOrDefault()!;
            }
            else
            {
                data = db.TDoctorTreatments.Where(a => a.Name == name && a.IsDelete == false && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        //[HttpPost("{Id}")]
        //public IActionResult UpdateUserProfileImage(int Id, [FromBody] string imagePath)
        //{
        //    var user = db.MBiodataAttachments.FirstOrDefault(a => a.Id == Id);
        //    if (user != null)
        //    {
        //        user.FileName = imagePath;
        //        db.SaveChanges();
        //        return Ok(new { Message = "Image path updated successfully" });
        //    }
        //    else
        //    {
        //        return NotFound();
        //    }
        //}

        [HttpGet("GetAllDataTreatment")]
        public List<VMTblTreatment> GetAllDataTreatment()
        {
            List<VMTblTreatment> data = (from z in db.TDoctorTreatments
                                         join y in db.MDoctors on z.DoctorId equals y.Id
                                         where z.DoctorId == y.Id && z.IsDelete == false
                                         select new VMTblTreatment
                                       {
                                           Id = z.Id,
                                           Name = z.Name,
                                           DoctorId = y.Id,

                                           IsDelete = z.IsDelete,
                                           CreatedOn = z.CreatedOn,
                                       }).ToList();

            return data;
        }

        [HttpGet("GetDataByIdTreatment/{Id}")]
        public TDoctorTreatment DataByIdTreatment(int id)
        {
            TDoctorTreatment result = db.TDoctorTreatments.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpPost("Save")]
        public VMResponse Save(TDoctorTreatment data)
        {
            //TDoctorTreatment dt = db.TDoctorTreatments.Where(a => a.Id == data.Id).FirstOrDefault()!;
            data.DoctorId = 1;
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;
            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;
        }

        [HttpDelete("Delete/{id}")]
        public VMResponse Delete(int id)
        {
            TDoctorTreatment dt = db.TDoctorTreatments.Where(a => a.Id == id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.ModifiedBy = IdUser;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success delete";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed delete : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }


    }
}
