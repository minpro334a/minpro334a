﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiSegmentationProductKesehatanController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiSegmentationProductKesehatanController(DB_SpecificationContext _db)
        {
            db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MMedicalItemSegmentation> GetAllData()
        {
            List<MMedicalItemSegmentation> data = db.MMedicalItemSegmentations.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{Id}")]
        public MMedicalItemSegmentation DataById(int id)
        {
            MMedicalItemSegmentation result = db.MMedicalItemSegmentations.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpGet("CheckCategoryByName/{name}/{id}")]
        public bool CheckName(string name, int id)
        {
            MMedicalItemSegmentation data = new MMedicalItemSegmentation();
            if (id == 0)
            {
                data = db.MMedicalItemSegmentations.Where(a => a.Name == name && a.IsDelete == false).FirstOrDefault()!;
            }
            else
            {
                data = db.MMedicalItemSegmentations.Where(a => a.Name == name && a.IsDelete == false && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpPost("Save")]
        public VMResponse Save(MMedicalItemSegmentation data)
        {
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(MMedicalItemSegmentation data)
        {
            MMedicalItemSegmentation dt = db.MMedicalItemSegmentations.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Name = data.Name;
                dt.ModifiedBy = data.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;

        }

        [HttpDelete("Delete/{Id}/{DeletedBy}")]
        public VMResponse Delete(int Id, int DeletedBy)
        {
            MMedicalItemSegmentation dt = db.MMedicalItemSegmentations.Where(a => a.Id == Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.IsDelete = true;
                dt.DeletedOn = DateTime.Now;
                dt.DeletedBy = DeletedBy;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success delete";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed delete : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }
    }
}
