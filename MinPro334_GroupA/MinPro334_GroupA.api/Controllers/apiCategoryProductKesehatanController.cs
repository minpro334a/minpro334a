﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCategoryProductKesehatanController : ControllerBase
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();

        public apiCategoryProductKesehatanController(DB_SpecificationContext _db)
        {
           db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MMedicalItemCategory> GetAllData()
        {
            List<MMedicalItemCategory> data = db.MMedicalItemCategories.Where(a => a.IsDelete == false).ToList();
            return data;
        }

        [HttpGet("GetDataById/{Id}")]
        public MMedicalItemCategory DataById(int id)
        {
            MMedicalItemCategory result = db.MMedicalItemCategories.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpGet("CheckCategoryByName/{name}/{id}")]
        public bool CheckName(string name, int id)
        {
            MMedicalItemCategory data = new MMedicalItemCategory();
            if (id == 0)
            {
                data = db.MMedicalItemCategories.Where(a => a.Name == name && a.IsDelete == false).FirstOrDefault()!;
            }
            else
            {
                data = db.MMedicalItemCategories.Where(a => a.Name == name && a.IsDelete == false && a.Id != id).FirstOrDefault()!;
            }

            if (data != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        [HttpPost("Save")]
        public VMResponse Save(MMedicalItemCategory data)
        {
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data);
                db.SaveChanges();

                respon.Message = "Data success saved";
            }
            catch (Exception ex)
            {
                respon.Success = false;
                respon.Message = "Failed saved : " + ex.Message;
            }

            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(MMedicalItemCategory data)
        {
            MMedicalItemCategory dt = db.MMedicalItemCategories.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.Name = data.Name;
                dt.ModifiedBy = data.ModifiedBy;
                dt.ModifiedOn = DateTime.Now;

                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success saved";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed saved : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;

        }

        [HttpDelete("Delete/{Id}/{DeletedBy}")]
        public VMResponse Delete(int Id, int DeletedBy)
        {
            MMedicalItemCategory dt = db.MMedicalItemCategories.Where(a => a.Id == Id).FirstOrDefault()!;

            if (dt != null)
            {
                dt.DeletedBy = DeletedBy;
                dt.IsDelete = true;
                dt.DeletedOn = DateTime.Now;


                try
                {
                    db.Update(dt);
                    db.SaveChanges();

                    respon.Message = "Data success delete";
                }
                catch (Exception ex)
                {
                    respon.Success = false;
                    respon.Message = "Failed delete : " + ex.Message;
                }
            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not found";
            }

            return respon;
        }
    }

}

