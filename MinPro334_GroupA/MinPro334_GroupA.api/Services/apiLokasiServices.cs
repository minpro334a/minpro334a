﻿using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Services
{
    public class apiLokasiServices
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();

        public apiLokasiServices(DB_SpecificationContext _db)
        {
            this.db = _db;
        }


        public async Task<VMTblLokasi> GetParentData(long? id)
        {
            //VMTblLokasi data = new VMTblLokasi();
            
            
                VMTblLokasi data = (from L2 in db.MLocations
                                    join LM2 in db.MLocationLevels on L2.LocationLevelId equals LM2.Id
                                    where L2.Id == id && L2.IsDelete == false
                                    select new VMTblLokasi
                                    {
                                        Id = L2.Id,
                                        Name = L2.Name,
                                        ParentChildName = L2.Name,
                                        ParentId = L2.ParentId ?? null,
                                        LocationLevelId = L2.LocationLevelId,
                                        NameLocation = LM2.Name,
                                    }).FirstOrDefault()!;

                if (data.ParentId != null)
                {
                    data.ParentData = await GetParentData(data.ParentId);

                    return data;
                }
                else
                {

                    return data;
                }
            

        }
    }
}

