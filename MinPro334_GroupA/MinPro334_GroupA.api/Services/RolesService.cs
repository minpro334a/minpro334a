﻿using Microsoft.AspNetCore.Mvc;
using MinPro334_GroupA.datamodels;
using MinPro334_GroupA.viewmodels;

namespace MinPro334_GroupA.api.Services
{
    public class RolesService
    {
        private readonly DB_SpecificationContext db;
        private VMResponse respon = new VMResponse();

        public RolesService(DB_SpecificationContext _db)
        {
            this.db = _db;
        }

        public async Task<List<VMMenuRole>> GetMenuAccessParentChildByRoleID(long IdRole, long? ParentId, bool OnlySelected = false)
        {
            List<VMMenuRole> result = new List<VMMenuRole>();
            List<MMenu> data = db.MMenus.Where(a => a.ParentId == ParentId && a.IsDelete == false).ToList();
            foreach (MMenu item in data)
            {
                VMMenuRole list = new VMMenuRole();
                list.MenuId = item.Id;
                list.MenuName = item.Name;
                list.ParentId = item.ParentId;

                list.is_selected = db.MMenuRoles.Where(a => a.RoleId == IdRole && a.MenuId == item.Id && a.IsDelete == false).Any();
                list.ListChild = await GetMenuAccessParentChildByRoleID(IdRole, item.Id, OnlySelected);
                if (OnlySelected)
                {
                    if (list.is_selected)
                        result.Add(list);
                }
                else
                {
                    result.Add(list);
                }
            }
            return result;
        }

    }
}
