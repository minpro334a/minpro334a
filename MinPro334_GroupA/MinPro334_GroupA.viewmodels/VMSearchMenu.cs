﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinPro334_GroupA.viewmodels
{
    public class VMSearchMenu
    {
        public string? Name { get; set; }
        public string? NameCategory { get; set; }        
        public string? Caution { get; set; }
        public long? PriceMax { get; set; }
        public long? PriceMin { get; set; }
    }
}
