﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinPro334_GroupA.viewmodels
{
    public class VMTblLokasi
    {
        public long Id { get; set; }    
        public string? Name { get; set; }
        public long? ParentId { get; set; }
        public string? NameLocation { get; set; }
        public string? ParentName { get; set; }
        public string? ParentChildName { get; set; }
        public long? LocationLevelId { get; set; }
        public long IdLocation { get; set; }
        public string Abbreviation { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
        public VMTblLokasi ParentData {  get; set; }

    }
}
