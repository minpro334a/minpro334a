﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinPro334_GroupA.viewmodels
{
    public class VMCariDokter
    {
        public long Id { get; set; }
        public string? Fullname { get; set; }

        public string? Location_Name { get; set; }

        public string? Specialization_Name { get; set; }

        public string? Hospital_Name { get; set; }

        public string? Treatment_Name { get; set; }

        public int Experience { get; set; }
        public long? DoctorId { get; set; }
        public string? ImagePath { get; set; }
        public string? Abbreviation { get; set; }

        public List<VMCariDokter>? ListMedicalFacility { get; set; }




        public string? Img { get; set; }
        public string Specialization { get; set; } = null!;
        public string? MedicalFacilityCategoryName { get; set; }
        public string? MedicalFacilityName { get; set; }
        public List<VMCariDokter> ListNameRs { get; set; }
    }
}
