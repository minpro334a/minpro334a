﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinPro334_GroupA.viewmodels
{
    public class VMCustomerMember
    {
        public long Id { get; set; }
        public long? ParentBiodataId { get; set; }
        public long? CustomerId { get; set; }
        public long? CustomerRelationId { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        //public long? ModifiedBy { get; set; }
        //public DateTime? ModifiedOn { get; set; }
        //public long? DeletedBy { get; set; }
        //public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }

        //Mbiodatum
        public string? Fullname { get; set; }

        //MCustomer
        public DateTime? Dob { get; set; }
        public int? Age { get; set; }
        public string? Gender { get; set; }
        public long? BloodGroupId { get; set; }
        public string? RhesusType { get; set; }
        public decimal? Height { get; set; }
        public decimal? Weight { get; set; }

        //MCustomerRelation
        public string? Name { get; set; }

        //blood
        public string? Code { get; set; }
    }
}
