﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinPro334_GroupA.viewmodels
{
    public class VMTblProfilDokter
    {
        public string? NameDoctor { get; set; }
        public string? Fullname { get; set; }
        public string? ListNameRs { get; set; }
        public string? MedicalFacilityName { get; set; }
        public string? Lulus { get; set; }
        public string? NameLevelLocation { get; set; }
        public string? Img { get; set; }
        public string? NameTreatment { get; set; }
        public string? NameHospital { get; set; }
        public string? Lokasi { get; set; }
        public string? SpecializationFaskes { get; set; }
        public string? InstitutionName { get; set; }    
        public string? Major { get; set; }
        public long? DoctorId { get; set; }
        public long? IdNameHospital { get; set; }
        public long? CollapseId { get; set; }
        public long? ScheduleId { get; set; }
        public long? IdAppointmentDone { get; set; }
        public long? IdAppointmentCancel { get; set; }
        public long? IdKonsul { get; set; }
        public string? Day { get; set; }
        public string? JamMulai { get; set; }
        public string? JamSelesai { get; set; }
        public decimal? PriceStartFrom { get; set; }
        public long? IdAppointment { get; set; }
        public long? TreatmentId { get; set; }
        public long? MedicalFacilityId { get; set; }
        public string? Specialization { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }

        public List<VMTblProfilDokter> ListNameTreatment { get; set; }
        public List<VMTblProfilDokter> ListJadwalPraktekWaktu { get; set; }
        public List<VMTblProfilDokter> ListNameHospital { get; set; }
        public List<VMTblProfilDokter> ListSpecializationFaskes { get; set; }
        public List<VMTblProfilDokter> ListInstitutionName { get; set; }
        public List<VMTblProfilDokter> ListJadwalPraktek { get; set; }
        public List<VMTblProfilDokter> ListKonsultasi { get; set; }
        public List<VMTblProfilDokter> ListJumlahJanji { get; set; }
        public List<VMTblProfilDokter> ListJanjiDone { get; set; }
        public List<VMTblProfilDokter> ListJanjiCancel { get; set; }
        public List<VMTblProfilDokter> ListLokasiPraktek { get; set; }
    }
}
