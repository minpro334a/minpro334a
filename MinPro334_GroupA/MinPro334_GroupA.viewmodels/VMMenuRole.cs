﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinPro334_GroupA.viewmodels
{
    public class VMMenuRole
    {
        public long Id { get; set; }
        public long? MenuId { get; set; }
        public long? RoleId { get; set; }
        public long? ModifiedBy { get; set; }


        public string? MenuName { get; set; } 
        public long? ParentId { get; set; }


        public bool is_selected { get; set; }
        public List<VMMenuRole>? ListChild { get; set; }

    }
}
