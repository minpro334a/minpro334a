﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinPro334_GroupA.viewmodels
{
    public class VMMMenuAccess
    {
        public string? Name { get; set; }
        public string? SmallIcon { get; set; }
        public string? BigIcon { get; set; }
        public long Id { get; set; }
        public long? ParentId { get; set; }
        public long? RoleId { get; set; }
        public string? Url { get; set; }

        public List<VMMMenuAccess>? ListChild { get; set; }
    }
}
