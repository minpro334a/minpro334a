﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MinPro334_GroupA.viewmodels
{
    public class VMSearchPage
    {
        public string? CodeTransaction { get; set; }
        public string? NameProduct { get; set; }
        public DateTime? MinDate { get; set; }
        public DateTime? MaxDate { get; set; }
        public decimal? MinAmount { get; set; }
        public decimal? MaxAmount { get; set; }
        public string? Caution { get; set; }
        public string? text { get; set; }

        public long? MedicalItemCategoryId { get; set; }
        public string? NameCategory { get; set; }

        public string? Fullname { get; set; }
        public string? Treatment_Name { get; set; }
        public string? Specialization_Name { get; set; }
        public string? FullAddress { get; set; }
  
        public string? Location_Name { get; set; }
        public string? Abbreviation { get; set; }
        public int? pageNumber { get; set; }
        public int? pageSize { get; set; }
        public int? CurrentPageSize { get; set; }
    }
}
